#!/usr/bin/env bash

set -e

for wat in src/test/resources/com/partisiablockchain/language/wasminvoker/*.wat; do
  wasm=${wat%.wat}.wasm
  echo "$wat $wasm"
  wat2wasm -o "$wasm" "$wat"
done
