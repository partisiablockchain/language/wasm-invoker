# Wasm invoker

Mediates between Wasm binders and the wasm interpreter, ensuring that input and
output formats are correctly handled.

