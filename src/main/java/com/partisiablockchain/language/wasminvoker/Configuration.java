package com.partisiablockchain.language.wasminvoker;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.Export;
import com.partisiablockchain.language.wasm.common.WasmModule;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Configuration of the wasm invoker that gives which features the invoker supports based on the
 * binder version.
 *
 * @param supportsReturnValues If true actions can return values and callbacks can receive returned
 *     values.
 * @param supportsCostFromContract If true events can be sent using gas from the contract.
 */
public record Configuration(boolean supportsReturnValues, boolean supportsCostFromContract) {

  /**
   * Default constructor.
   *
   * @param supportsReturnValues If true actions can return values and callbacks can receive
   *     returned values.
   * @param supportsCostFromContract If true events can be sent using gas from the contract.
   */
  public Configuration {}

  /**
   * Parses a wasm module to find the binder version.
   *
   * @param module the module to parse
   * @return a configuration based on binder version found
   */
  public static SemVer determineBinderVersion(WasmModule module) {
    SemVer foundVersion = null;
    for (final Export export : module.getExportSection()) {
      final var version = parseWasmExportToString(export.getName());
      if (version == null) {
        continue;
      }
      if (foundVersion != null) {
        throw new RuntimeException(
            String.format(
                "Inconsistent binder ABI version for WASM program: Found multiple version fields:"
                    + " %s and %s",
                foundVersion, version));
      }
      foundVersion = version;
    }
    return foundVersion;
  }

  /** Assumes that both versions given are not null. */
  static boolean isVersionSupportedByBinderWithVersion(
      final SemVer moduleVersion, final SemVer binderVersion) {
    return binderVersion.major() == moduleVersion.major()
        && binderVersion.minor() >= moduleVersion.minor();
  }

  /**
   * Checks whether the given version is supported by any of the given supported binder versions.
   *
   * @param moduleVersion Version of parsed module
   * @param supportedBinderVersions Collection of supported versions
   */
  public static void assertBinderSupportsVersion(
      final SemVer moduleVersion, final Collection<SemVer> supportedBinderVersions) {
    if (moduleVersion == null) {
      throw new RuntimeException("Could not determine binder ABI version for WASM program");
    }
    if (supportedBinderVersions.stream()
        .noneMatch(
            supportedVersion ->
                isVersionSupportedByBinderWithVersion(moduleVersion, supportedVersion))) {
      throw new RuntimeException(
          String.format(
              "Unsupported binder ABI version %s in WASM. Supported versions are %s",
              moduleVersion, formatSupportedVersions(supportedBinderVersions)));
    }
  }

  private static String formatSupportedVersions(final Collection<SemVer> supportedBinderVersions) {
    return "["
        + supportedBinderVersions.stream()
            .map(v -> String.format("%d.0.0-%d.%d.X", v.major(), v.major(), v.minor()))
            .collect(Collectors.joining(", "))
        + "]";
  }

  /** Prefix for version fields in {@link WasmModule}. */
  private static final String VERSION_PREFIX = "__PBC_VERSION_BINDER_";

  /** Pattern for version fields in {@link WasmModule}. */
  private static final Pattern VERSION_PATTERN =
      Pattern.compile(VERSION_PREFIX + "(?<major>\\d+)_(?<minor>\\d+)_(?<patch>\\d+)");

  /**
   * Parses version from an WASM export name.
   *
   * @return null if this is not a version of the expected format.
   */
  static SemVer parseWasmExportToString(String name) {
    Matcher matcher = VERSION_PATTERN.matcher(name);
    if (matcher.find()) {
      int major = checkBounds(Integer.parseInt(matcher.group("major")));
      int minor = checkBounds(Integer.parseInt(matcher.group("minor")));
      int patch = checkBounds(Integer.parseInt(matcher.group("patch")));

      return new SemVer(major, minor, patch);
    }
    return null;
  }

  static int checkBounds(int version) {
    if (version >= 256) {
      throw new RuntimeException("Byte value out of bounds: " + version);
    }
    return version;
  }
}
