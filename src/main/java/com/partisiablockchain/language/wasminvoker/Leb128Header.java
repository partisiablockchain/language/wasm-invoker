package com.partisiablockchain.language.wasminvoker;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.stream.SafeDataInputStream;

/** A small LEB128 utility. */
public final class Leb128Header {

  private Leb128Header() {}

  /**
   * Read the RPC looking for a single LEB128 number of maximum 32 bits, which is 5 bytes. If it
   * exceeds 5 bytes an error is thrown.
   *
   * @param rpc the RPC to read from
   * @return the header length
   * @throws IllegalArgumentException on invalid input
   */
  public static int readHeaderLength(byte[] rpc) {
    var maxLength = Math.min(rpc.length, 5);
    for (int i = 0; i < maxLength; i++) {
      if ((rpc[i] & 0x80) == 0) {
        return i + 1;
      }
    }

    throw new IllegalArgumentException(
        "RPC header must be a 32-bit LEB128 encoded int (max 5 bytes).");
  }

  /**
   * Reads LEB-128 bytes from a stream, and returns the bytes raw as they were in the stream.
   *
   * @param stream Stream to read from
   * @return array of LEB-128 bytes. At most 5 bytes long
   */
  public static byte[] readHeaderBytes(final SafeDataInputStream stream) {
    final byte[] headerBytes = new byte[5];
    int len = 0;
    while (true) {
      final byte streamByte = stream.readSignedByte();
      headerBytes[len++] = streamByte;
      if ((streamByte & 0x80) == 0) {
        break;
      } else if (len >= 5) {
        throw new IllegalArgumentException(
            "RPC header must be a 32-bit LEB128 encoded int (max 5 bytes).");
      }
    }

    return java.util.Arrays.copyOf(headerBytes, len);
  }
}
