package com.partisiablockchain.language.wasminvoker.events;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.List;

/**
 * Calculates the amount of gas to use for events/callbacks with no allocated cost in EventResults.
 */
final class EventCostCalculator {
  private EventCostCalculator() {}

  /**
   * Consists of a sum of already allocated gas costs and the number of events/callbacks with cost
   * from sender and no allocated cost.
   *
   * @param allocatedCost The total cost already allocated to events/callbacks.
   * @param numOfUnallocatedCosts The number of events/callbacks that will share the remaining gas,
   *     since they have no allocated cost
   */
  record CostSum(long allocatedCost, int numOfUnallocatedCosts) {
    CostSum add(CostSum that) {
      return new CostSum(
          allocatedCost + that.allocatedCost(),
          numOfUnallocatedCosts + that.numOfUnallocatedCosts());
    }
  }

  /**
   * Calculates the gas to be allocated to events and callbacks with cost from sender and no
   * allocated cost. If all events and callbacks have already been allocated gas, returns zero.
   *
   * @param eventResult to calculate already allocated gas for
   * @param availableGas Gas available to distribute
   * @return the gas cost to allocate to each event/callback with unallocated cost
   */
  static long calculateGasCostIfUnallocated(EventResult eventResult, long availableGas) {
    CostSum costSum = calculateCost(eventResult.getInvocations());

    if (eventResult.getCallbackGroup() != null) {
      costSum = costSum.add(calculateCost(eventResult.getCallbackGroup()));
    }

    long costLeftForEvents = availableGas - costSum.allocatedCost();
    if (costLeftForEvents < 0) {
      throw new RuntimeException(
          String.format(
              "Cannot allocate gas for events. Total cost for events is %d available gas is %d",
              costSum.allocatedCost(), availableGas));
    }

    if (costSum.numOfUnallocatedCosts > 0) {
      return costLeftForEvents / costSum.numOfUnallocatedCosts();
    } else {
      return 0L;
    }
  }

  /**
   * Calculates cost for a list of events.
   *
   * @param events to calculate for
   * @return the CostSum of the events
   */
  static CostSum calculateCost(List<EventResult.Event> events) {
    int numEventsWithUnallocatedCost =
        (int)
            events.stream()
                .filter(event -> !event.costFromContract())
                .filter(EventResult.Event::hasNullCost)
                .count();

    long allocatedCost =
        events.stream()
            .filter(event -> !event.costFromContract())
            .map(EventResult.Event::getCostOrZero)
            .reduce(Long::sum)
            .orElse(0L);

    return new CostSum(allocatedCost, numEventsWithUnallocatedCost);
  }

  /**
   * Calculates cost for a callback group.
   *
   * @param callbackGroup to calculate for
   * @return the CostSum of the callback group
   */
  static CostSum calculateCost(EventResult.CallbackGroup callbackGroup) {
    CostSum costSum = calculateCost(callbackGroup.events());
    if (callbackGroup.callbackCost() == null) {
      costSum = costSum.add(new CostSum(0L, 1));
    } else if (!callbackGroup.costFromContract()) {
      costSum = costSum.add(new CostSum(callbackGroup.callbackCost(), 0));
    }
    return costSum;
  }
}
