package com.partisiablockchain.language.wasminvoker.events;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.binder.BinderEventGroup;
import com.partisiablockchain.binder.BinderInteraction;
import com.partisiablockchain.binder.BinderResult;
import com.partisiablockchain.binder.CallResult;
import java.util.List;

/**
 * Converts mutable EventResults to immutable BinderResults while setting gas costs wherever these
 * have not been allocated.
 */
public final class BinderResultConverter {

  private BinderResultConverter() {}

  /**
   * Creates a BinderResult from an EventResult. Distributes all available gas between events and
   * callbacks that have not yet been allocated gas. Parameterized with a state type since zk and
   * public contracts have different state types.
   *
   * @param eventResult to create binder result from
   * @param <StateT> the contract state type
   * @param newState the new state resulting from the invocation
   * @param availableGas the gas to be distributed among events and callbacks
   * @return new binder result
   */
  public static <StateT> BinderResult<StateT, BinderInteraction> toBinderResult(
      EventResult eventResult, StateT newState, long availableGas) {
    long gasCostIfUnallocated =
        EventCostCalculator.calculateGasCostIfUnallocated(eventResult, availableGas);
    List<BinderInteraction> invocations =
        toBinderInteractions(eventResult.getInvocations(), gasCostIfUnallocated);

    CallResult callResult;
    if (eventResult.getCallReturnValue() != null) {
      callResult = eventResult.getCallReturnValue();
    } else if (eventResult.getCallbackGroup() != null) {
      callResult = toBinderEventGroup(eventResult.getCallbackGroup(), gasCostIfUnallocated);
    } else {
      callResult = null;
    }

    return new BinderResult<>() {
      @Override
      public List<BinderInteraction> getInvocations() {
        return invocations;
      }

      @Override
      public CallResult getCallResult() {
        return callResult;
      }

      @Override
      public StateT getState() {
        return newState;
      }
    };
  }

  /**
   * Converts a list of events to a list of binder interaction.
   *
   * @param events to convert
   * @param gasCostIfUnallocated the event cost to allocate to events without allocated cost
   * @return new list of binder interactions
   */
  static List<BinderInteraction> toBinderInteractions(
      List<EventResult.Event> events, long gasCostIfUnallocated) {
    return events.stream().map(e -> toBinderInteraction(e, gasCostIfUnallocated)).toList();
  }

  /**
   * Converts an event to a binder interaction.
   *
   * @param event to convert
   * @param gasCostIfUnallocated the event cost to allocate if the event has no allocated cost
   * @return new binder interaction
   */
  static BinderInteraction toBinderInteraction(EventResult.Event event, long gasCostIfUnallocated) {
    long effectiveCost = event.hasNullCost() ? gasCostIfUnallocated : event.cost();
    return new BinderInteraction(
        event.contract(), event.rpc(), false, effectiveCost, event.costFromContract());
  }

  /**
   * Converts a callback group to an immutable BinderEventGroup.
   *
   * @param callbackGroup to convert
   * @param gasCostIfUnallocated the event cost to allocate to events and callback without allocated
   *     cost
   * @return new immutable event group
   */
  static BinderEventGroup<BinderInteraction> toBinderEventGroup(
      EventResult.CallbackGroup callbackGroup, long gasCostIfUnallocated) {
    long effectiveCallbackCost =
        callbackGroup.callbackCost() == null ? gasCostIfUnallocated : callbackGroup.callbackCost();
    return new BinderEventGroup<>(
        callbackGroup.callbackRpc(),
        effectiveCallbackCost,
        callbackGroup.costFromContract(),
        toBinderInteractions(callbackGroup.events(), gasCostIfUnallocated));
  }
}
