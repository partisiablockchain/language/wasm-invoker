package com.partisiablockchain.language.wasminvoker.events;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.binder.CallReturnValue;
import com.partisiablockchain.language.wasminvoker.Configuration;
import com.secata.stream.SafeDataInputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * The wasm invocation produces a stream of serialized event groups. Each event group contains
 * serialized events, a possible callback rpc and a possible return value. This class reads this
 * stream and converts it to an EventResult.
 *
 * @see EventResult
 */
public final class SerializedEventResult {

  private SerializedEventResult() {}

  /**
   * Reads an event result from stream.
   *
   * @param configuration the configuration of the wasm invoker
   * @param stream Stream to read from
   * @return immutable list of MutableEventGroup s
   */
  public static EventResult readEventResultFromStream(
      Configuration configuration, final SafeDataInputStream stream) {
    final List<EventResult.Event> invocations = new ArrayList<>();
    EventResult.CallbackGroup callbackGroup = null;
    CallReturnValue callReturnValue = null;

    int groupCount = stream.readInt();
    for (int j = 0; j < groupCount; j++) {
      var callbackRpc = stream.readOptional(SafeDataInputStream::readDynamicBytes);
      var callbackCost = stream.readOptional(SafeDataInputStream::readLong);

      List<EventResult.Event> interactions = new ArrayList<>();
      var interactionCount = stream.readInt();
      for (var i = 0; i < interactionCount; i++) {
        interactions.add(readEvent(configuration, stream));
      }

      if (callbackRpc != null) {
        if (callbackGroup != null) {
          throw new RuntimeException("Multiple callback groups are not allowed");
        }
        callbackGroup =
            new EventResult.CallbackGroup(interactions, callbackRpc, callbackCost, false);
      } else {
        invocations.addAll(interactions);
      }

      if (configuration.supportsReturnValues()) {
        var returnData = stream.readOptional(SafeDataInputStream::readDynamicBytes);
        if (returnData != null) {
          if (callReturnValue != null) {
            throw new RuntimeException("Multiple return values are not allowed");
          }
          callReturnValue = new CallReturnValue(returnData);
        }
      }
    }
    return new EventResult(invocations, callbackGroup, callReturnValue);
  }

  /**
   * Reads a single event from the stream. The following is read from the stream:
   *
   * <ul>
   *   <li>destination: A blockchain address to send the event to
   *   <li>rawPayload: The serialized payload of the event
   *   <li>fromOriginalSender: Flag set if the event is sent from the original sender of the
   *       invocation. Not supported.
   *   <li>effectiveCost: Gas cost associated with the event
   * </ul>
   *
   * @param configuration the configuration of the wasm invoker
   * @param in the stream to read from
   * @return the read event
   */
  static EventResult.Event readEvent(Configuration configuration, SafeDataInputStream in) {
    var destination = BlockchainAddress.read(in);
    var rawPayload = in.readDynamicBytes();
    boolean fromOriginalSender = in.readBoolean();
    if (fromOriginalSender) {
      throw new RuntimeException("Sending from original sender is not supported");
    }
    var effectiveCost = in.readOptional(SafeDataInputStream::readLong);
    boolean costFromContract = false;
    if (configuration.supportsCostFromContract()) {
      costFromContract = in.readBoolean();
    }
    return new EventResult.Event(destination, rawPayload, effectiveCost, costFromContract);
  }
}
