package com.partisiablockchain.language.wasminvoker.events;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.binder.BinderInteraction;
import com.partisiablockchain.binder.BinderResult;
import com.partisiablockchain.binder.CallReturnValue;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Mutable, intermediary version of BinderResult containing information about the events and return
 * value resulting from an invocation. Consists of a list of fire-and-forget invocations, a
 * (possibly null) callback group and a (possibly null) return value. After creation, the result can
 * be mutated by adding new fire-and-forget invocations and by setting the result. Lastly, the
 * result can be finalized, creating an immutable binder result.
 */
public final class EventResult {

  private final List<Event> invocations;
  private final CallbackGroup callbackGroup;
  private CallReturnValue callReturnValue;

  /**
   * Primary constructor. Is called either through the empty constructor or through
   * SerializedEventResult.
   *
   * @param invocations fire-and-forget invocations. Must not be null
   * @param callbackGroup callback group
   * @param callReturnValue return value
   * @throws RuntimeException if both callbackGroup and callReturnValue is provided
   */
  EventResult(
      List<Event> invocations, CallbackGroup callbackGroup, CallReturnValue callReturnValue) {
    List<Event> events = Objects.requireNonNull(invocations);
    this.invocations = new ArrayList<>(events);
    if (callbackGroup != null && callReturnValue != null) {
      throw new RuntimeException("Having both return value and callback group is not allowed.");
    }
    this.callbackGroup = callbackGroup;
    this.callReturnValue = callReturnValue;
  }

  /**
   * Creates an empty event result.
   *
   * @return EventResult with no invocations, callback group or return value.
   */
  public static EventResult create() {
    return new EventResult(new ArrayList<>(), null, null);
  }

  /**
   * Return the fire-and-forget invocations of this result.
   *
   * @return the invocations
   */
  public List<Event> getInvocations() {
    return List.copyOf(invocations);
  }

  /**
   * Returns the callback group of this result. A callback group consists of a list of events, a
   * callback rpc and a gas cost to pay for the callback rpc. The callback rpc is executed after all
   * the events have been executed.
   *
   * @return the callback group
   */
  public CallbackGroup getCallbackGroup() {
    return callbackGroup;
  }

  /**
   * Returns the return value of this result. The return value is passed to the contract or user
   * making the invocation.
   *
   * @return the return value
   */
  public CallReturnValue getCallReturnValue() {
    return callReturnValue;
  }

  /**
   * Adds a fire-and-forget event to this result.
   *
   * @param contract the contract to send the event to
   * @param rpc the payload of the event
   * @param cost the gas cost allocated to the event. If set to null, a gas cost is automatically
   *     allocated from the remaining gas when this EventResult is finalized.
   */
  public void addEventWithCostFromContract(BlockchainAddress contract, byte[] rpc, long cost) {
    Event event = new Event(contract, rpc, cost, true);
    invocations.add(event);
  }

  /**
   * Adds a return value to this result.
   *
   * @param returnValue the return value to add
   * @throws RuntimeException if a return value or callback group already exists
   */
  public void setReturnValue(byte[] returnValue) {
    if (callbackGroup != null) {
      throw new RuntimeException("Having both return value and callback group is not allowed.");
    }
    if (callReturnValue != null) {
      throw new RuntimeException("Cannot overwrite return value");
    }
    callReturnValue = new CallReturnValue(returnValue);
  }

  /**
   * Converts this mutable event result into an immutable BinderResult which is the final result of
   * the current invocation.
   *
   * @param <StateT> the contract state type
   * @param newState the new state resulting from the invocation
   * @param availableGas the gas to be distributed among events and callbacks
   * @return the finalized binder result
   */
  public <StateT> BinderResult<StateT, BinderInteraction> toBinderResult(
      StateT newState, long availableGas) {
    return BinderResultConverter.toBinderResult(this, newState, availableGas);
  }

  /**
   * An event to execute after the current invocation.
   *
   * @param contract the contract to send the event to
   * @param rpc the payload of the event
   * @param cost the gas cost allocated to the event. If set to null, a gas cost is automatically
   *     allocated from the remaining gas when the event result is finalized
   * @param costFromContract if true the cost is paid from the contract. If false the cost is paid
   *     from the sender of the current invocation
   */
  @SuppressWarnings("ArrayRecordComponent")
  public record Event(BlockchainAddress contract, byte[] rpc, Long cost, boolean costFromContract) {

    /**
     * Constructor.
     *
     * @param contract the contract to send the event to
     * @param rpc the payload of the event
     * @param cost the gas cost allocated to the event. If set to null, a gas cost is automatically
     *     allocated from the remaining gas when the event result is finalized
     * @param costFromContract if true the cost is paid from the contract. If false the cost is paid
     *     from the sender of the current invocation
     */
    public Event {
      assertNotNegative(cost);
    }

    boolean hasNullCost() {
      return cost == null;
    }

    long getCostOrZero() {
      return Objects.requireNonNullElse(cost, 0L);
    }
  }

  /**
   * A callback group to execute after the current invocation. A callback group consists of a list
   * of events, a callback rpc and a gas cost to pay for the callback rpc. The callback rpc is
   * executed after all the events have been executed.
   *
   * @param events the events associated with the callback
   * @param callbackRpc the payload of the callback sent after the events
   * @param callbackCost the gas cost allocated to the callback. If set to null, a gas cost is
   *     automatically allocated from the remaining gas when the event result is finalized
   * @param costFromContract if true the cost is paid from the contract. If false the cost is paid
   *     from the sender of the current invocation
   */
  @SuppressWarnings("ArrayRecordComponent")
  public record CallbackGroup(
      List<Event> events, byte[] callbackRpc, Long callbackCost, boolean costFromContract) {

    /**
     * Constructor.
     *
     * @param events the events associated with the callback
     * @param callbackRpc the payload of the callback sent after the events
     * @param callbackCost the gas cost allocated to the callback. If set to null, a gas cost is
     *     automatically allocated from the remaining gas when the event result is finalized
     * @param costFromContract if true the cost is paid from the contract. If false the cost is paid
     *     from the sender of the current invocation
     */
    public CallbackGroup {
      assertNotEmpty(events);
      Objects.requireNonNull(callbackRpc);
      assertNotNegative(callbackCost);
    }
  }

  private static void assertNotNegative(Long cost) {
    if (cost != null && ((Number) cost).longValue() < 0) {
      throw new RuntimeException("Negative cost is not allowed");
    }
  }

  private static void assertNotEmpty(List<Event> events) {
    Objects.requireNonNull(events);
    if (events.isEmpty()) {
      throw new RuntimeException("Empty event list is not allowed");
    }
  }
}
