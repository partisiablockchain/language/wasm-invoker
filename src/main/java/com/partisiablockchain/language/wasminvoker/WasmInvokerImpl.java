package com.partisiablockchain.language.wasminvoker;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.SharedContext;
import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.WasmLimitations;
import com.partisiablockchain.language.wasm.common.WasmModule;
import com.partisiablockchain.language.wasm.common.literal.Int32;
import com.partisiablockchain.language.wasm.common.literal.Int64;
import com.partisiablockchain.language.wasm.common.literal.Literal;
import com.partisiablockchain.language.wasm.interpreter.WasmInstance;
import com.partisiablockchain.language.wasm.interpreter.WasmMemory;
import com.partisiablockchain.language.wasm.parser.WasmParser;
import com.partisiablockchain.language.wasm.pbclib.HostedExit;
import com.partisiablockchain.language.wasm.pbclib.HostedMemmove;
import com.partisiablockchain.language.wasminvoker.avltrees.AvlTreeWrapper;
import com.partisiablockchain.language.wasminvoker.avltrees.AvlTrees;
import com.partisiablockchain.language.wasminvoker.avltrees.HostedAvlTree;
import com.partisiablockchain.language.wasminvoker.events.EventResult;
import com.partisiablockchain.language.wasminvoker.events.SerializedEventResult;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

/** The default implementation of the {@link WasmInvoker}. */
public final class WasmInvokerImpl implements WasmInvoker {

  private static final WasmLimitations limitations = WasmLimitations.DEFAULT;
  private final WasmModule module;
  private final Configuration config;

  WasmInvokerImpl(final WasmModule module, final Configuration config) {
    this.module = module;
    this.config = config;
  }

  /**
   * Constructs a new {@link WasmInvoker} from bytes representing the {@link WasmModule}.
   *
   * @param wasmBytes Bytes representing the WASM module to create an invoker for
   * @param config Configuration to use with the given wasm bytes.
   * @return A new {@link WasmInvoker} for the given {@link WasmModule}.
   */
  public static WasmInvoker fromWasmBytes(final byte[] wasmBytes, final Configuration config) {
    final WasmParser parser = new WasmParser(wasmBytes);
    return fromModule(parser.parse(), config);
  }

  /**
   * Constructs a new {@link WasmInvoker} from a parsed {@link WasmModule}.
   *
   * @param wasmModule WASM module to create an invoker for
   * @param config Configuration to use with the given wasm module.
   * @return A new {@link WasmInvoker} for the given {@link WasmModule}.
   */
  public static WasmInvoker fromModule(final WasmModule wasmModule, final Configuration config) {
    return new WasmInvokerImpl(wasmModule, config);
  }

  @Override
  public boolean doesFunctionExist(final String functionName) {
    return this.module.exportedFunctionsByName().containsKey(functionName);
  }

  @Override
  public WasmResult executeWasm(
      long availableGas,
      String functionName,
      SharedContext context,
      CallbackContext callbackContext,
      AvlTrees avlTrees,
      byte[]... params) {
    HostedAvlTree hostedAvlTree =
        avlTrees == null
            ? new HostedAvlTree(null)
            : new HostedAvlTree(avlTrees.avlTrees(), avlTrees.readOnlyAvlTrees());

    final WasmInstance instance = WasmInstance.forModule(module, limitations);
    instance.registerExternal("memmove", new HostedMemmove(new Uint31(100), Uint31.ZERO));
    instance.registerExternal("exit", new HostedExit());
    hostedAvlTree.registerAvlFunctions(instance);

    byte[][] effectiveParams = getEffectiveParams(config, context, callbackContext, params);

    final WasmMemory memory = instance.getMemory(Uint31.ZERO);
    final Uint31 basePointer = memory.size();
    memory.grow(calculatePagesNeeded(effectiveParams));

    Uint31 bufferLength = Uint31.ZERO;
    for (final byte[] parameter : effectiveParams) {
      final Uint31 parameterLength = new Uint31(parameter.length);
      memory.write(basePointer.add(bufferLength), parameter);
      bufferLength = bufferLength.add(parameterLength);
    }

    final List<Literal> arguments =
        List.of(new Int32(basePointer.asInt()), new Int32(bufferLength.asInt()));

    long availableCycles = availableGas * INSTRUCTION_CYCLES_PER_GAS;
    final List<Literal> result = instance.runFunction(functionName, availableCycles, arguments);
    if (result.isEmpty()) {
      throw new RuntimeException("No result");
    }

    Uint31 ptrToResult = new Uint31(((Int64) result.get(0)).toInt32());
    byte[] lengthInBytes = memory.read(ptrToResult, Uint31.FOUR);

    Uint31 resultAddress = ptrToResult.add(Uint31.FOUR);
    Uint31 resultLength = new Uint31(createBeBuffer(lengthInBytes).getInt());

    // Currently, in the wasm instance, each unit of cycles pays for one instruction
    // Hence the cycles used in the wasm instance equals the number of instructions
    long cyclesUsed = instance.getCyclesUsed();
    long gasUsed = convertToGas(cyclesUsed);

    byte[] rawResult = memory.read(resultAddress, resultLength);
    return createResult(config, gasUsed, rawResult, hostedAvlTree.getAvlTrees());
  }

  private static ByteBuffer createBeBuffer(byte[] rawResult) {
    return ByteBuffer.wrap(rawResult).order(ByteOrder.BIG_ENDIAN);
  }

  /**
   * Retrieves a section from a list of result sections.
   *
   * @param sectionMap sections to search in
   * @param wantedSectionId id of section to look for
   * @return null if the section weren't in the given list
   */
  public static WasmResultSection getSection(
      final Map<Byte, byte[]> sectionMap, final byte wantedSectionId) {
    final byte[] sectionData = sectionMap.getOrDefault(wantedSectionId, null);
    if (sectionData == null) {
      return null;
    }
    return new WasmResultSection(wantedSectionId, sectionData);
  }

  /**
   * Convert the supplied instructions to gas given {@link #INSTRUCTION_CYCLES_PER_GAS}. Rounds up
   * the used gas.
   *
   * @param usedCycles the number of instructions used
   * @return the gas equivalent of the instructions
   */
  public static long convertToGas(long usedCycles) {
    return (usedCycles + INSTRUCTION_CYCLES_PER_GAS - 1) / INSTRUCTION_CYCLES_PER_GAS;
  }

  private static final Set<Byte> SECTIONS_WELL_KNOWN =
      Set.of(WasmResultSection.SECTION_ID_EVENTS, WasmResultSection.SECTION_ID_STATE);

  static WasmResult createResult(
      Configuration configuration,
      final long usedGas,
      final byte[] rawResult,
      AvlTree<Integer, AvlTreeWrapper> avlTrees) {
    final Map<Byte, byte[]> sectionMap = SectionParser.parseSections(rawResult);

    // Parse wasm state
    final WasmResultSection stateSection =
        getSection(sectionMap, WasmResultSection.SECTION_ID_STATE);
    final byte[] rawState = stateSection == null ? new byte[0] : stateSection.sectionData();
    WasmState wasmState = new WasmState(new LargeByteArray(rawState), avlTrees);

    // Parse event groups
    final WasmResultSection eventGroupSection =
        getSection(sectionMap, WasmResultSection.SECTION_ID_EVENTS);
    final EventResult eventResult;
    if (eventGroupSection == null) {
      eventResult = EventResult.create();
    } else {
      eventResult =
          SafeDataInputStream.deserialize(
              safeDataInputStream ->
                  SerializedEventResult.readEventResultFromStream(
                      configuration, safeDataInputStream),
              eventGroupSection.sectionData());
    }

    // Create list of sections that are not well-known
    final List<WasmResultSection> unknownResultSections =
        sectionMap.entrySet().stream()
            .map(e -> new WasmResultSection(e.getKey(), e.getValue()))
            .filter(s -> !SECTIONS_WELL_KNOWN.contains(s.sectionId()))
            .toList();

    return new WasmResult(wasmState, eventResult, usedGas, unknownResultSections);
  }

  private Uint31 calculatePagesNeeded(final byte[][] bytes) {
    Uint31 result = Uint31.ZERO;
    for (final byte[] array : bytes) {
      result = result.add(new Uint31(array.length));
    }
    return result.div(WasmMemory.PAGE_SIZE).addOne();
  }

  /**
   * Serializes a shared context into bytes.
   *
   * @param context to context to be serialized
   * @return the serialized context
   */
  public static byte[] serializeSharedContext(SharedContext context) {
    return SafeDataOutputStream.serialize(
        out -> {
          context.getContractAddress().write(out);
          context.getFrom().write(out);
          out.writeLong(context.getBlockTime());
          out.writeLong(context.getBlockProductionTime());
          context.getCurrentTransactionHash().write(out);
          context.getOriginalTransactionHash().write(out);
        });
  }

  /**
   * Serializes a callback context into bytes under a Configuration.
   *
   * @param callbackContext the callback context to be serialized
   * @param config the configuration under which the callback context is serialized
   * @return the serialized callback context
   */
  public static byte[] serializeCallbackContext(
      CallbackContext callbackContext, Configuration config) {
    return SafeDataOutputStream.serialize(
        out -> {
          out.writeBoolean(callbackContext.isSuccess());

          out.writeInt(callbackContext.results().size());
          for (CallbackContext.ExecutionResult result : callbackContext.results()) {
            var succeeded = result.isSucceeded();
            out.writeBoolean(succeeded);
            if (config.supportsReturnValues()) {
              var resultDataStream = result.returnValue().readAllBytes();
              out.writeDynamicBytes(resultDataStream);
            }
          }
        });
  }

  /**
   * Serializes the context and the callback context under a configuration and concats them with the
   * rest of the params.
   *
   * @param config the configuration under which to serialize
   * @param context the context of the invocation
   * @param callbackContext the callback context of the function, should be null when the function
   *     is not a callback
   * @param params the rest of the function parameters
   * @return the effective parameters
   */
  public static byte[][] getEffectiveParams(
      Configuration config,
      SharedContext context,
      CallbackContext callbackContext,
      byte[][] params) {
    ArrayList<byte[]> effectiveParams = new ArrayList<>();
    effectiveParams.add(serializeSharedContext(context));
    if (callbackContext != null) {
      effectiveParams.add(serializeCallbackContext(callbackContext, config));
    }
    effectiveParams.addAll(Arrays.stream(params).toList());
    return effectiveParams.toArray(new byte[0][]);
  }
}
