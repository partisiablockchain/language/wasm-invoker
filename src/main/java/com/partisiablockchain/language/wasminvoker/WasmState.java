package com.partisiablockchain.language.wasminvoker;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.language.wasminvoker.avltrees.AvlTreeWrapper;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import java.util.Objects;

/** The state class for all WASM contracts. */
@Immutable
public final class WasmState implements StateSerializable {

  private final LargeByteArray state;
  private final AvlTree<Integer, AvlTreeWrapper> avlTrees;

  /** Serializable constructor. */
  public WasmState() {
    this.state = null;
    this.avlTrees = null;
  }

  /**
   * Default constructor.
   *
   * @param state initial wasm state
   */
  public WasmState(LargeByteArray state) {
    this.state = Objects.requireNonNull(state);
    this.avlTrees = AvlTree.create();
  }

  /**
   * Constructor.
   *
   * @param state initial wasm state
   * @param avlTrees avl trees
   */
  public WasmState(LargeByteArray state, AvlTree<Integer, AvlTreeWrapper> avlTrees) {
    this.state = Objects.requireNonNull(state);
    this.avlTrees = Objects.requireNonNullElseGet(avlTrees, AvlTree::create);
  }

  /**
   * Get wasm state.
   *
   * @return state as byte array.
   */
  public byte[] getState() {
    return state.getData();
  }

  /**
   * Get avl trees.
   *
   * @return avl trees
   */
  public AvlTree<Integer, AvlTreeWrapper> getAvlTrees() {
    return avlTrees;
  }

  /**
   * Creates next state with given wasm state bytes.
   *
   * @param state bytes for next state
   * @return next state
   */
  public WasmState withState(byte[] state) {
    return new WasmState(new LargeByteArray(state), avlTrees);
  }
}
