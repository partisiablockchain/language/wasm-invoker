package com.partisiablockchain.language.wasminvoker;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Semantic version tracking.
 *
 * <p>The binder version `X.Y.Z` supports SDK versions `X.W.O`, for `Y >= W`. That is:
 *
 * <ul>
 *   <li>Major version: Backwards incompatible changes, no mix and matching.
 *   <li>Minor version: Forwards incompatible changes. A new SDK running on an old binder could use
 *       functionality that the binder doesn't know about, so this is not supported.
 *   <li>Patch version: Both backwards and forwards compatible changes. Anything goes.
 * </ul>
 */
public record SemVer(int major, int minor, int patch) {
  @Override
  public String toString() {
    return String.format("%d.%d.%d", major, minor, patch);
  }
}
