package com.partisiablockchain.language.wasminvoker;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.SharedContext;
import com.partisiablockchain.language.wasminvoker.avltrees.AvlTrees;
import com.partisiablockchain.language.wasminvoker.events.EventResult;
import java.util.List;

/** An interface that defines the WASM API. */
public interface WasmInvoker {

  /**
   * Checks whether an exported function with the given name exists.
   *
   * @param functionName the function name to check
   * @return true if and only if a function with the given name exists
   */
  boolean doesFunctionExist(String functionName);

  /**
   * Execute the given function from WASM.
   *
   * @param availableWasmGas the gas available for the execution
   * @param functionName the function to run
   * @param context the context of the function
   * @param callbackContext the callback context of the function, should be null when the function
   *     is not a callback
   * @param avlTrees initial stored avl trees
   * @param params the rest of the parameters for the function
   * @return the result of the execution
   */
  WasmResult executeWasm(
      long availableWasmGas,
      String functionName,
      SharedContext context,
      CallbackContext callbackContext,
      AvlTrees avlTrees,
      byte[]... params);

  /** Result of the wasm execution. */
  record WasmResult(
      WasmState state,
      EventResult eventResult,
      long usedWasmGas,
      List<WasmResultSection> sections) {

    /**
     * Retrieves a section from the result.
     *
     * @param wantedSectionId id of section to look for
     * @return null if the section weren't in the result
     */
    public WasmResultSection getSection(final byte wantedSectionId) {
      for (final WasmResultSection section : this.sections) {
        if (section.sectionId == wantedSectionId) {
          return section;
        }
      }
      return null;
    }
  }

  /** Section of the WasmResult. */
  @SuppressWarnings("ArrayRecordComponent")
  record WasmResultSection(byte sectionId, byte[] sectionData) {

    /** Section id of the state section. */
    public static final byte SECTION_ID_STATE = 0x01;

    /** Section id of the events section. */
    public static final byte SECTION_ID_EVENTS = 0x02;
  }

  /**
   * Number of instructions per gas to pay. WASM requires a lot of instructions, hence we grant 1000
   * instructions pr unit of gas.
   */
  int INSTRUCTION_CYCLES_PER_GAS = 1_000;
}
