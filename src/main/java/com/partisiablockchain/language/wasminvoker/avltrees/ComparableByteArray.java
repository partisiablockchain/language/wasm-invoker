package com.partisiablockchain.language.wasminvoker.avltrees;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateSerializable;
import java.util.Arrays;

/** Comparable byte array. Used as keys in an AvlTree. */
@Immutable
public final class ComparableByteArray
    implements Comparable<ComparableByteArray>, StateSerializable {
  private final LargeByteArray data;

  /** Serializable constructor. */
  ComparableByteArray() {
    this.data = null;
  }

  /**
   * Create comparable byte array.
   *
   * @param data inner byte array
   */
  public ComparableByteArray(byte[] data) {
    this.data = new LargeByteArray(data);
  }

  /**
   * Get the data as bytes.
   *
   * @return data as bytes
   */
  public byte[] getData() {
    return this.data.getData();
  }

  /**
   * Get the byte length of data.
   *
   * @return byte length of data
   */
  public int getLength() {
    return data.getLength();
  }

  @Override
  public int compareTo(ComparableByteArray o) {
    return Arrays.compareUnsigned(getData(), o.getData());
  }
}
