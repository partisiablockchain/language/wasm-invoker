package com.partisiablockchain.language.wasminvoker.avltrees;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import java.util.Map;

/** Common interface for read only avl trees used in HostedAvlTree. */
@Immutable
public interface ReadOnlyAvlTreeMapper {

  /**
   * Gets the corresponding value to given key.
   *
   * @param key key
   * @return value
   */
  byte[] getValue(byte[] key);

  /**
   * Gets the size of the corresponding value to given key.
   *
   * @param key key
   * @return size of the value. Returns -1 if there is no corresponding value.
   */
  int getValueSize(byte[] key);

  /**
   * The number of key value pairs in the tree.
   *
   * @return the number of key value pairs
   */
  int size();

  /**
   * Get (key, value) for the entry following the provided key. To get the first entry use the null
   * key.
   *
   * @param key The initial key. If null, the method returns the entry.
   * @return The next entry. Returns null if there is no next entry.
   */
  Map.Entry<byte[], byte[]> getNextEntry(byte[] key);

  /**
   * Gets the size of the entry following the provided key. To get the size of the first entry use
   * the null key.
   *
   * @param key key
   * @return size of the next entry. Returns -1 if there is no next entry.
   */
  int getNextEntrySize(byte[] key);
}
