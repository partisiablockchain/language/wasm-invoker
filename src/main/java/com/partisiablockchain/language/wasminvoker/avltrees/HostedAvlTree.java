package com.partisiablockchain.language.wasminvoker.avltrees;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.literal.Int32;
import com.partisiablockchain.language.wasm.common.literal.Literal;
import com.partisiablockchain.language.wasm.interpreter.WasmInstance;
import com.partisiablockchain.language.wasm.interpreter.WasmMemory;
import com.partisiablockchain.tree.AvlTree;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;

/**
 * AvlTrees that live outside the Wasm runtime. Defines external functions that are called from the
 * Wasm runtime to modify the hosted avl trees.
 */
public final class HostedAvlTree {

  private AvlTree<Integer, AvlTreeWrapper> avlTrees;
  private final AvlTree<Integer, ReadOnlyAvlTreeMapper> readOnlyAvlTrees;
  static final Uint31 INSTRUCTION_CYCLES_PER_CALL = new Uint31(100000);

  /**
   * Create a new Hosted Avl Tree.
   *
   * @param avlTrees the initial avl trees
   */
  public HostedAvlTree(AvlTree<Integer, AvlTreeWrapper> avlTrees) {
    this.avlTrees = Objects.requireNonNullElseGet(avlTrees, AvlTree::create);
    this.readOnlyAvlTrees = null;
  }

  /**
   * Create a new Hosted Avl Tree.
   *
   * @param avlTrees the initial avl trees
   * @param readOnlyAvlTrees the read only avl trees
   */
  public HostedAvlTree(
      AvlTree<Integer, AvlTreeWrapper> avlTrees,
      AvlTree<Integer, ReadOnlyAvlTreeMapper> readOnlyAvlTrees) {
    this.avlTrees = Objects.requireNonNullElseGet(avlTrees, AvlTree::create);
    this.readOnlyAvlTrees = readOnlyAvlTrees;
  }

  /**
   * Registers all external avl functions.
   *
   * @param instance the instance on which to register.
   */
  public void registerAvlFunctions(WasmInstance instance) {
    instance.registerExternal("avl_new", this.wasmAvlNew());
    instance.registerExternal("avl_insert", this.wasmAvlInsert());
    instance.registerExternal("avl_get_size", this.wasmAvlGetSize());
    instance.registerExternal("avl_get", this.wasmAvlGet());
    instance.registerExternal("avl_remove", this.wasmAvlRemove());
    instance.registerExternal("avl_len", this.wasmAvlLen());
    instance.registerExternal("avl_get_next_size", this.wasmAvlGetNextSize());
    instance.registerExternal("avl_get_next", this.wasmAvlGetNext());
  }

  /**
   * Create a new avl tree with a fresh tree id.
   *
   * @return the tree id of the created avl tree.
   */
  int avlNew() {
    int treeId = nextTreeId();
    avlTrees = avlTrees.set(treeId, AvlTreeWrapper.create());
    return treeId;
  }

  /**
   * Get the value corresponding to the given key in the avl tree with id treeId.
   *
   * @param treeId id of the tree to lookup.
   * @param key the key.
   * @return the corresponding value.
   */
  byte[] avlGet(int treeId, byte[] key) {
    return getTreeAsReadOnly(treeId).getValue(key);
  }

  /**
   * Get the size of the value corresponding to the given key in the avl tree with id treeId.
   *
   * @param treeId id of the tree to lookup.
   * @param key the key.
   * @return the size of the corresponding value.
   */
  int avlGetValueSize(int treeId, byte[] key) {
    return getTreeAsReadOnly(treeId).getValueSize(key);
  }

  /**
   * Inserts (key, value) into the avl tree with id treeId.
   *
   * @param treeId the tree id of the tree to modify.
   * @param key the key of the entry to insert into.
   * @param value the value to insert.
   */
  void avlInsert(int treeId, byte[] key, byte[] value) {
    assertModifiableTree(treeId);
    AvlTreeWrapper newSubTree = avlTrees.getValue(treeId).set(key, value);
    avlTrees = avlTrees.set(treeId, newSubTree);
  }

  /**
   * Removes the entry with key from the avl tree with id treeId.
   *
   * @param treeId the tree id of the tree to modify.
   * @param key the key of the entry to remove.
   */
  void avlRemove(int treeId, byte[] key) {
    assertModifiableTree(treeId);
    AvlTreeWrapper newSubTree = avlTrees.getValue(treeId).remove(key);
    avlTrees = avlTrees.set(treeId, newSubTree);
  }

  /**
   * Gets the size of avl tree with id treeId.
   *
   * @param treeId the id of the tree.
   * @return the size of the tree.
   */
  int avlSize(int treeId) {
    return getTreeAsReadOnly(treeId).size();
  }

  /**
   * Gets the entry in the tree with tree id following the provided key.
   *
   * @param treeId the id of the tree.
   * @param key the key. If null gets the first entry.
   * @return the following entry. If there is no next, returns null.
   */
  Map.Entry<byte[], byte[]> avlGetNext(int treeId, byte[] key) {
    return getTreeAsReadOnly(treeId).getNextEntry(key);
  }

  /**
   * Gets the size of the entry in the tree with tree id following the provided key.
   *
   * @param treeId the id of the tree.
   * @param key the key. If null gets the first entry.
   * @return the size of the following entry. If there is no next, returns -1.
   */
  int avlGetNextEntrySize(int treeId, byte[] key) {
    return getTreeAsReadOnly(treeId).getNextEntrySize(key);
  }

  /**
   * Gets the next free tree id.
   *
   * @return the next tree id.
   */
  private int nextTreeId() {
    return avlTrees.keySet().stream().max(Integer::compareTo).orElse(-1) + 1;
  }

  /**
   * Get tree from tree id. If tree id is less than zero, get read only tree, else get normal user
   * defined tree.
   *
   * @param treeId tree id.
   * @return tree.
   */
  private ReadOnlyAvlTreeMapper getTreeAsReadOnly(int treeId) {
    if (treeId < 0) {
      return readOnlyAvlTrees.getValue(treeId);
    } else {
      return avlTrees.getValue(treeId);
    }
  }

  /**
   * Only tree ids greater than or equal to zero are modifiable.
   *
   * @param treeId tree id
   */
  private static void assertModifiableTree(int treeId) {
    if (treeId < 0) {
      throw new RuntimeException("Can't modify read only avl trees");
    }
  }

  /**
   * Get the avl trees.
   *
   * @return the avl trees
   */
  public AvlTree<Integer, AvlTreeWrapper> getAvlTrees() {
    return avlTrees;
  }

  /**
   * Creates a new avl tree with fresh tree id. The wasm function takes no inputs and returns the
   * tree id.
   *
   * @return external avl_new function
   */
  public Function<WasmInstance, List<Literal>> wasmAvlNew() {
    return instance -> {
      int treeId = avlNew();
      instance.useCycles(INSTRUCTION_CYCLES_PER_CALL);
      return List.of(new Int32(treeId));
    };
  }

  /**
   * Inserts key, value into avl tree with given tree id. The wasm function takes as inputs the tree
   * id, as well as pointers and data length to the key and value. It returns void.
   *
   * @return external avl_insert function
   */
  public Function<WasmInstance, List<Literal>> wasmAvlInsert() {
    return instance -> {
      final Int32 treeIdRaw = (Int32) instance.getLocal(new Uint31(0));
      final Int32 keyPtrRaw = (Int32) instance.getLocal(new Uint31(1));
      final Int32 keyLenRaw = (Int32) instance.getLocal(new Uint31(2));
      final Int32 valuePtrRaw = (Int32) instance.getLocal(new Uint31(3));
      final Int32 valueLenRaw = (Int32) instance.getLocal(new Uint31(4));

      int treeId = treeIdRaw.value();

      final WasmMemory memory = instance.getMemory(Uint31.ZERO);

      final byte[] key = readPtr(memory, keyPtrRaw, keyLenRaw, "Reading avl_insert key");
      final byte[] value = readPtr(memory, valuePtrRaw, valueLenRaw, "Reading avl_insert value");
      avlInsert(treeId, key, value);

      instance.useCycles(INSTRUCTION_CYCLES_PER_CALL);
      return List.of();
    };
  }

  /**
   * Get the size of the value corresponding to the given key and tree id. The wasm function takes
   * as input the tree id and a pointer and data length of the key. It returns the size of the value
   * or -1 if the value is not present.
   *
   * @return external avl_get_size function
   */
  public Function<WasmInstance, List<Literal>> wasmAvlGetSize() {
    return instance -> {
      final Int32 treeIdRaw = (Int32) instance.getLocal(new Uint31(0));
      final Int32 keyPtrRaw = (Int32) instance.getLocal(new Uint31(1));
      final Int32 keyLenRaw = (Int32) instance.getLocal(new Uint31(2));

      int treeId = treeIdRaw.value();

      WasmMemory memory = instance.getMemory(new Uint31(0));
      final byte[] key = readPtr(memory, keyPtrRaw, keyLenRaw, "Reading avl_get_size key");

      final int size = avlGetValueSize(treeId, key);

      instance.useCycles(INSTRUCTION_CYCLES_PER_CALL);
      return List.of(new Int32(size));
    };
  }

  /**
   * Gets the value corresponding to the given key and tree id. The wasm function takes as input the
   * tree id and a pointer and data length of the key, as well as a destination pointer. Writes the
   * value to the destination pointer. Assumes a byte array has already been initialized with the
   * correct size at the destination. The wasm function returns a bool whether the key was present
   * in the avl tree.
   *
   * @return external avl_get function
   */
  public Function<WasmInstance, List<Literal>> wasmAvlGet() {
    return instance -> {
      final Int32 treeIdRaw = (Int32) instance.getLocal(new Uint31(0));
      final Int32 keyPtrRaw = (Int32) instance.getLocal(new Uint31(1));
      final Int32 keyLenRaw = (Int32) instance.getLocal(new Uint31(2));
      final Int32 dstPtrRaw = (Int32) instance.getLocal(new Uint31(3));

      int treeId = treeIdRaw.value();

      WasmMemory memory = instance.getMemory(new Uint31(0));
      final byte[] key = readPtr(memory, keyPtrRaw, keyLenRaw, "Reading avl_get key");

      final byte[] value = avlGet(treeId, key);

      instance.useCycles(INSTRUCTION_CYCLES_PER_CALL);
      if (value == null) {
        return List.of(new Int32(0));
      } else {
        writeToPtr(memory, dstPtrRaw, "Writing avl_get value", value);
        return List.of(new Int32(1));
      }
    };
  }

  /**
   * Removes the entry corresponding to the given key and tree id. The wasm function takes as input
   * the tree id and a pointer and data length of the key. It returns void.
   *
   * @return external avl_remove function
   */
  public Function<WasmInstance, List<Literal>> wasmAvlRemove() {
    return instance -> {
      final Int32 treeIdRaw = (Int32) instance.getLocal(new Uint31(0));
      final Int32 keyPtrRaw = (Int32) instance.getLocal(new Uint31(1));
      final Int32 keyLenRaw = (Int32) instance.getLocal(new Uint31(2));

      int treeId = treeIdRaw.value();
      WasmMemory memory = instance.getMemory(new Uint31(0));
      final byte[] key = readPtr(memory, keyPtrRaw, keyLenRaw, "Reading avl_remove key");

      avlRemove(treeId, key);

      instance.useCycles(INSTRUCTION_CYCLES_PER_CALL);
      return List.of();
    };
  }

  /**
   * Gets the size of avl tree with tree id. The wasm function takes as input the tree id and
   * returns the size of the tree.
   *
   * @return external avl_len function
   */
  public Function<WasmInstance, List<Literal>> wasmAvlLen() {
    return instance -> {
      final Int32 treeIdRaw = (Int32) instance.getLocal(new Uint31(0));

      int treeId = treeIdRaw.value();

      instance.useCycles(INSTRUCTION_CYCLES_PER_CALL);
      return List.of(new Int32(avlSize(treeId)));
    };
  }

  /**
   * Get the size of the key value pair the given key and tree id. The wasm function takes as input
   * the tree id and a pointer and data length of the key. It returns the size of the key value pair
   * or -1 if the value is not present.
   *
   * @return external avl_get_next_size function
   */
  public Function<WasmInstance, List<Literal>> wasmAvlGetNextSize() {
    return instance -> {
      final Int32 treeIdRaw = (Int32) instance.getLocal(new Uint31(0));
      final Int32 keyPtrRaw = (Int32) instance.getLocal(new Uint31(1));
      final Int32 keyLenRaw = (Int32) instance.getLocal(new Uint31(2));

      int treeId = treeIdRaw.value();

      final WasmMemory memory = instance.getMemory(Uint31.ZERO);
      byte[] key = readPtrOrNull(memory, keyPtrRaw, keyLenRaw, "Reading avl_get_next_size key");

      final int size = avlGetNextEntrySize(treeId, key);

      instance.useCycles(INSTRUCTION_CYCLES_PER_CALL);
      return List.of(new Int32(size));
    };
  }

  /**
   * Gets the key value pair after the given key and tree id. The wasm function takes as input the
   * tree id and a pointer and data length of the key, as well as a destination pointer. Writes the
   * key value pair to the destination pointer. Assumes a byte array has already been initialized
   * with the correct size at the destination. The wasm function returns a bool whether the there is
   * a next entry in the avl tree.
   *
   * @return external avl_get_next function
   */
  public Function<WasmInstance, List<Literal>> wasmAvlGetNext() {
    return instance -> {
      final Int32 treeIdRaw = (Int32) instance.getLocal(new Uint31(0));
      final Int32 keyPtrRaw = (Int32) instance.getLocal(new Uint31(1));
      final Int32 keyLenRaw = (Int32) instance.getLocal(new Uint31(2));
      final Int32 dstPtrRaw = (Int32) instance.getLocal(new Uint31(3));

      int treeId = treeIdRaw.value();

      final WasmMemory memory = instance.getMemory(Uint31.ZERO);
      byte[] key = readPtrOrNull(memory, keyPtrRaw, keyLenRaw, "Reading avl_get_next key");

      final Map.Entry<byte[], byte[]> entry = avlGetNext(treeId, key);

      instance.useCycles(INSTRUCTION_CYCLES_PER_CALL);
      if (entry == null) {
        return List.of(new Int32(0));
      } else {
        writeToPtr(
            memory, dstPtrRaw, "Writing avl_get_next value", entry.getKey(), entry.getValue());

        return List.of(new Int32(1));
      }
    };
  }

  private static byte[] readPtr(
      WasmMemory memory, Int32 keyPtrRaw, Int32 keyLenRaw, String errorMsg) {
    final Uint31 keyPtr = keyPtrRaw.toUint31OrNull();
    final Uint31 keyLen = keyLenRaw.toUint31OrNull();

    if (keyPtr == null || keyLen == null || !memory.isWithinBounds(keyPtr, keyLen)) {
      throw memory.newBoundsChecksException(keyPtrRaw.value(), keyLenRaw.value(), errorMsg);
    }
    return memory.read(keyPtr, keyLen);
  }

  private static byte[] readPtrOrNull(
      WasmMemory memory, Int32 keyPtrRaw, Int32 keyLenRaw, String errorMsg) {
    final Uint31 keyPtr = keyPtrRaw.toUint31OrNull();
    final Uint31 keyLen = keyLenRaw.toUint31OrNull();

    if (keyPtr == null || keyLen == null) {
      return null;
    }
    if (!memory.isWithinBounds(keyPtr, keyLen)) {
      throw memory.newBoundsChecksException(keyPtrRaw.value(), keyLenRaw.value(), errorMsg);
    }
    return memory.read(keyPtr, keyLen);
  }

  private static void writeToPtr(
      WasmMemory memory, Int32 dstPtrRaw, String errorMsg, byte[]... data) {
    final Uint31 dstPtr = dstPtrRaw.toUint31OrNull();
    Uint31 length = Uint31.ZERO;
    for (final byte[] array : data) {
      length = length.add(new Uint31(array.length));
    }

    if (dstPtr == null || !memory.isWithinBounds(dstPtr, length)) {
      throw memory.newBoundsChecksException(dstPtrRaw.value(), length.asInt(), errorMsg);
    }

    Uint31 bufferLength = Uint31.ZERO;
    for (final byte[] array : data) {
      final Uint31 parameterLength = new Uint31(array.length);
      memory.write(dstPtr.add(bufferLength), array);
      bufferLength = bufferLength.add(parameterLength);
    }
  }
}
