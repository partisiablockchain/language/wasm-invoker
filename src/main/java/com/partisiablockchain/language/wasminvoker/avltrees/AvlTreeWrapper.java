package com.partisiablockchain.language.wasminvoker.avltrees;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import java.util.List;
import java.util.Map;

/**
 * Simple wrapper of an AvlTree to allow state serialization of {@link
 * com.partisiablockchain.language.wasminvoker.WasmState WasmState}. Required as it is not allowed
 * to have nested parameterized types for state serialization.
 */
@Immutable
public final class AvlTreeWrapper implements StateSerializable, ReadOnlyAvlTreeMapper {
  private final AvlTree<ComparableByteArray, LargeByteArray> avlTree;

  /** Serializable constructor. */
  AvlTreeWrapper() {
    this.avlTree = null;
  }

  /**
   * Default constructor.
   *
   * @param avlTree inner AvlTree
   */
  public AvlTreeWrapper(AvlTree<ComparableByteArray, LargeByteArray> avlTree) {
    this.avlTree = avlTree;
  }

  /**
   * Constructs a new empty AvlTree. Delegates to AvlTree.
   *
   * @return new empty tree
   */
  public static AvlTreeWrapper create() {
    return new AvlTreeWrapper(AvlTree.create());
  }

  /**
   * Associates the specified value with the specified key. Delegates to AvlTree.
   *
   * @param key key with which the specified value is to be associated
   * @param value value to be associated with the specified key
   * @return new tree with key and associated value
   */
  public AvlTreeWrapper set(byte[] key, byte[] value) {
    return new AvlTreeWrapper(avlTree.set(new ComparableByteArray(key), new LargeByteArray(value)));
  }

  /**
   * Removes the mapping for a key from this tree if it is present. Delegates to AvlTree.
   *
   * @param key key whose mapping is to be removed from the tree
   * @return new tree without key and associated value
   */
  public AvlTreeWrapper remove(byte[] key) {
    return new AvlTreeWrapper(avlTree.remove(new ComparableByteArray(key)));
  }

  /**
   * Get the inner avl tree.
   *
   * @return the inner avl tree.
   */
  public AvlTree<ComparableByteArray, LargeByteArray> getAvlTree() {
    return avlTree;
  }

  @Override
  public byte[] getValue(byte[] key) {
    LargeByteArray value = avlTree.getValue(new ComparableByteArray(key));
    return value == null ? null : value.getData();
  }

  @Override
  public int getValueSize(byte[] key) {
    LargeByteArray value = avlTree.getValue(new ComparableByteArray(key));
    return value == null ? -1 : value.getLength();
  }

  @Override
  public int size() {
    return avlTree.size();
  }

  @Override
  public Map.Entry<byte[], byte[]> getNextEntry(byte[] key) {
    List<Map.Entry<ComparableByteArray, LargeByteArray>> entry =
        avlTree.getNextN(key == null ? null : new ComparableByteArray(key), 1);
    if (entry.isEmpty()) {
      return null;
    }
    return Map.entry(entry.get(0).getKey().getData(), entry.get(0).getValue().getData());
  }

  @Override
  public int getNextEntrySize(byte[] key) {
    List<Map.Entry<ComparableByteArray, LargeByteArray>> entry =
        avlTree.getNextN(key == null ? null : new ComparableByteArray(key), 1);
    if (entry.isEmpty()) {
      return -1;
    }
    return entry.get(0).getKey().getLength() + entry.get(0).getValue().getLength();
  }
}
