package com.partisiablockchain.language.wasminvoker;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/** Utility class for parsing sectioned binary data. */
public final class SectionParser {

  private SectionParser() {}

  /**
   * Parses sectioned binary data to a hashmap from ids to byte array. Sections must come in
   * ascending id order.
   *
   * @param inputBytes Bytes to parse.
   * @exception RuntimeException If the input bytes is encoded incorrectly.
   * @return Map from id byte to byte array of data.
   */
  public static Map<Byte, byte[]> parseSections(final byte[] inputBytes) {
    // Used for reading BE integers
    final var byteBuffer =
        java.nio.ByteBuffer.wrap(inputBytes).order(java.nio.ByteOrder.BIG_ENDIAN);

    // Initialize result
    final var sections = new HashMap<Byte, byte[]>();

    int srcPos = 0;
    byte previousSectionId = -1;
    while (srcPos < inputBytes.length) {
      final byte sectionId = byteBuffer.get(srcPos++);

      if (sectionId <= previousSectionId) {
        // Note that this also catches the cases where `sectionId < 0`.
        throw new RuntimeException(
            String.format(
                "Invalid section %d: Duplicated or incorrectly ordered. Expected section"
                    + " with id of at least %d",
                sectionId, previousSectionId + 1));
      }

      final int sectionLen = byteBuffer.getInt(srcPos);
      srcPos += 4;

      if (sectionLen < 0) {
        throw new RuntimeException(
            String.format(
                "Invalid section %d: Section length %d is negative", sectionId, sectionLen));
      }

      if (inputBytes.length - srcPos < sectionLen) {
        throw new RuntimeException(
            String.format(
                "Invalid section %d: Section length %d longer than remaining data",
                sectionId, sectionLen));
      }

      sections.put(sectionId, Arrays.copyOfRange(inputBytes, srcPos, srcPos + sectionLen));

      // Bookkeeping
      previousSectionId = sectionId;
      srcPos += sectionLen;
    }

    return Map.copyOf(sections);
  }
}
