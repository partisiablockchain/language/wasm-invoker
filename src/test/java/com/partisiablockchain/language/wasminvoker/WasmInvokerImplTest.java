package com.partisiablockchain.language.wasminvoker;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.binder.CallReturnValue;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.language.wasm.common.WasmModule;
import com.partisiablockchain.language.wasm.parser.WasmParser;
import com.partisiablockchain.language.wasminvoker.avltrees.AvlTreeWrapper;
import com.partisiablockchain.language.wasminvoker.avltrees.AvlTrees;
import com.partisiablockchain.language.wasminvoker.events.EventResult;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.LittleEndianByteInput;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.immutable.FixedList;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.HexFormat;
import java.util.List;
import java.util.Objects;
import org.bouncycastle.util.Arrays;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test of {@link WasmInvokerImpl}. */
public final class WasmInvokerImplTest {

  private byte[] b1;
  private byte[] b2;

  /** Set up the test. */
  @BeforeEach
  public void setUp() {
    b1 = new byte[] {1, 3, 3, 7, 7, 3, 3, 1};
    b2 = new byte[] {4, 2, 4, 2, 2, 4, 2, 4};
  }

  private static final Configuration CONFIG_V7 = new Configuration(false, false);
  private static final Configuration CONFIG_V8 = new Configuration(true, false);
  private static final List<SemVer> SUPPORTED_VERSIONS =
      List.of(new SemVer(7, 0, 0), new SemVer(8, 1, 0), new SemVer(9, 5, 0));

  @Test
  void init() {
    final WasmModule wasmModule = new WasmParser(load()).parse();
    final SemVer parsedVersion = Configuration.determineBinderVersion(wasmModule);
    Configuration.assertBinderSupportsVersion(parsedVersion, SUPPORTED_VERSIONS);
    final WasmInvoker invoker = WasmInvokerImpl.fromModule(wasmModule, CONFIG_V8);

    assertThat(invoker.doesFunctionExist("init")).isTrue();
    assertThat(invoker.doesFunctionExist("some_unknown_function")).isFalse();
    WasmInvoker.WasmResult main =
        invoker.executeWasm(10000, "init", new TestSharedContext(), null, null);
    byte[] state = main.state().getState();
    assertThat(state).contains("Hello World WASM".getBytes(StandardCharsets.UTF_8));
    assertThat(state)
        .containsExactly(
            (byte) 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 72, 101, 108, 108, 111, 32, 87,
            111, 114, 108, 100, 32, 87, 65, 83, 77);
  }

  @Test
  void wasmResultValues() {
    WasmState state = new WasmState();
    EventResult eventResult = EventResult.create();
    long usedWasmGas = 0L;
    List<WasmInvoker.WasmResultSection> sections = List.of();

    WasmInvoker.WasmResult wasmResult =
        new WasmInvoker.WasmResult(state, eventResult, usedWasmGas, sections);
    assertThat(wasmResult.state()).isEqualTo(state);
    assertThat(wasmResult.eventResult()).isEqualTo(eventResult);
    assertThat(wasmResult.usedWasmGas()).isEqualTo(usedWasmGas);
    assertThat(wasmResult.sections()).isEqualTo(sections);
  }

  @Test
  void invokeAction() {
    WasmInvoker invoker = WasmInvokerImpl.fromWasmBytes(load(), CONFIG_V8);
    assertThatThrownBy(
            () ->
                invoker.executeWasm(
                    10000, "action_a125a953", new TestSharedContext(), null, null, b2, b1))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("Invalid section 1: Duplicated or incorrectly ordered")
        .hasMessageContaining("Expected section with id of at least 3");
  }

  @Test
  void invokeNoResult() {
    WasmInvoker invoker = WasmInvokerImpl.fromWasmBytes(load(), CONFIG_V8);
    assertThatThrownBy(
            () -> invoker.executeWasm(10000, "noresult", new TestSharedContext(), null, null, b2))
        .hasMessage("No result");
  }

  void invokeAndTestWasmBinderVersionValid(final WasmInvoker invoker) {
    var result = invoker.executeWasm(10000, "noresult", new TestSharedContext(), null, null);
    assertThat(result.state().getState()).containsExactly("WASM".getBytes(StandardCharsets.UTF_8));
  }

  @Test
  void correctVersion() {
    invokeAndTestWasmBinderVersionValid(
        WasmInvokerImpl.fromWasmBytes(load("wasm-binder-version-valid.wasm"), CONFIG_V8));
  }

  @Test
  void correctVersionNoClientVersion() {
    invokeAndTestWasmBinderVersionValid(
        WasmInvokerImpl.fromWasmBytes(load("wasm-binder-version-valid-no-client.wasm"), CONFIG_V8));
  }

  @Test
  void realContract() {
    WasmModule module = new WasmParser(load("token_contract.wasm")).parse();
    final SemVer version = Configuration.determineBinderVersion(module);
    assertThat(version).isEqualTo(new SemVer(8, 0, 0));
  }

  @Test
  void incorrectVersion() {
    final WasmModule wasmModule =
        new WasmParser(load("wasm-binder-version-255.100.50.wasm")).parse();
    final SemVer parsedVersion = Configuration.determineBinderVersion(wasmModule);

    assertThatThrownBy(
            () -> Configuration.assertBinderSupportsVersion(parsedVersion, SUPPORTED_VERSIONS))
        .hasMessage(
            "Unsupported binder ABI version 255.100.50 in WASM. Supported versions are"
                + " [7.0.0-7.0.X, 8.0.0-8.1.X, 9.0.0-9.5.X]");
  }

  @Test
  void nonsenseMultipleVersionFields() {
    final WasmModule wasmModule = new WasmParser(load("wasm-binder-version-multiple.wasm")).parse();

    assertThatThrownBy(() -> Configuration.determineBinderVersion(wasmModule))
        .hasMessage(
            "Inconsistent binder ABI version for WASM program: Found multiple version fields: 1.0.7"
                + " and 2.0.0");
  }

  @Test
  void nullVersion() {
    WasmModule module = new WasmParser(load("wasm-binder-version-null.wasm")).parse();
    SemVer version = Configuration.determineBinderVersion(module);
    assertThat(version).isNull();
  }

  @Test
  void nullVersionTryRun() {
    final WasmModule wasmModule = new WasmParser(load("wasm-binder-version-null.wasm")).parse();
    final SemVer parsedVersion = Configuration.determineBinderVersion(wasmModule);
    assertThatThrownBy(() -> Configuration.assertBinderSupportsVersion(parsedVersion, List.of()))
        .hasMessage("Could not determine binder ABI version for WASM program");
  }

  @Test
  void testMemmoveIntegration() {
    final WasmInvoker invoker =
        WasmInvokerImpl.fromWasmBytes(load("wasm-memmove-test.wasm"), CONFIG_V8);
    final WasmInvoker.WasmResult main =
        invoker.executeWasm(10000, "call_memmove", new TestSharedContext(), null, null);
    final byte[] state = main.state().getState();
    assertThat(state).containsExactly("Hello, Hello!".getBytes(StandardCharsets.UTF_8));
  }

  @Test
  void testExitIntegration() {
    final WasmInvoker invoker =
        WasmInvokerImpl.fromWasmBytes(load("wasm-exit-test.wasm"), CONFIG_V8);
    assertThatThrownBy(
            () -> invoker.executeWasm(10000, "call_exit", new TestSharedContext(), null, null))
        .hasMessage(
            """
            Trap: Early exit: Exit string
                at hosted:exit():0
                at call_exit():3
                at call_call_exit():1""");
  }

  /**
   * Tests contracts_callback compiled with sdk version 9.1.2 into binder version 7.0.0.
   *
   * <p><a
   * href="https://gitlab.com/partisiablockchain/language/contracts/testing-types/-/blob/main/contract-callbacks/src/lib.rs">Contract
   * source</a>
   *
   * <p>The test calls an action that creates a callback event, and thereafter calls the callback.
   * There are no return values.
   */
  @Test
  void testActionNoReturn7() {
    WasmInvoker invoker =
        WasmInvokerImpl.fromWasmBytes(load("contract_callbacks-7-0-0.wasm"), CONFIG_V7);
    testContractCallbacks(invoker);
  }

  /**
   * Tests contracts_callback compiled with sdk version 11.0.0 into binder version 8.1.0.
   *
   * <p><a
   * href="https://gitlab.com/partisiablockchain/language/contracts/testing-types/-/blob/main/contract-callbacks/src/lib.rs">Contract
   * source</a>
   *
   * <p>The test calls an action that creates a callback event, and thereafter calls the callback.
   * There are no return values.
   */
  @Test
  void testActionNoReturn8dot1() {
    WasmInvoker invoker =
        WasmInvokerImpl.fromWasmBytes(load("contract_callbacks-8-1-0.wasm"), CONFIG_V8);
    testContractCallbacks(invoker);
  }

  private static void testContractCallbacks(WasmInvoker invoker) {
    var context = new TestSharedContext();
    WasmInvoker.WasmResult init =
        invoker.executeWasm(10000, "init", context, null, null, new byte[] {1, 0, 0, 0});
    byte[] initState = init.state().getState();
    assertThat(SafeDataInputStream.createFromBytes(initState).readInt()).isEqualTo(1);
    invoker.executeWasm(
        10000, "action_01", context, null, null, initState, new byte[] {0, 0, 0, 42});

    var executionResult =
        CallbackContext.createResult(
            context.getOriginalTransactionHash(),
            true,
            SafeDataInputStream.createFromBytes(new byte[0]));
    var callbackContext = CallbackContext.create(FixedList.create(List.of(executionResult)));
    WasmInvoker.WasmResult callback =
        invoker.executeWasm(
            10000,
            "callback_05",
            context,
            callbackContext,
            null,
            initState,
            new byte[] {0, 0, 0, 42});
    byte[] callbackState = callback.state().getState();
    assertThat(callbackState).contains(42);
  }

  /**
   * Tests contract_return_data_returner compiled with sdk version 10.0.0 into binder version 8.0.0.
   *
   * <p><a
   * href="https://gitlab.com/partisiablockchain/language/contracts/testing-types/-/blob/main/contract-return-data-returner/src/lib.rs">Contract
   * source</a>
   *
   * <p>The test calls an action that returns a value, and asserts that the returned value is
   * correct.
   */
  @Test
  void testActionReturn8() {
    WasmInvoker invoker =
        WasmInvokerImpl.fromWasmBytes(load("contract_return_data_returner-8-0-0.wasm"), CONFIG_V8);
    var context = new TestSharedContext();
    WasmInvoker.WasmResult init = invoker.executeWasm(10000, "init", context, null, null);
    byte[] initState = init.state().getState();
    initState[0] = 42;
    WasmInvoker.WasmResult provideU5 =
        invoker.executeWasm(10000, "action_02", context, null, null, initState);
    var callResult =
        (CallReturnValue) WasmResultParsingTest.toBinderResult(provideU5, 10000).getCallResult();
    var result = SafeDataInputStream.createFromBytes(callResult.getResult()).readLong();
    assertThat(result).isEqualTo(42);
  }

  /**
   * Tests contract_return_data_caller compiled with sdk version 10.0.0 into binder version 8.0.0.
   *
   * <p><a
   * href="https://gitlab.com/partisiablockchain/language/contracts/testing-types/-/blob/main/contract-return-data-caller/src/lib.rs">Contract
   * source</a>
   *
   * <p>The test builds a callback context with return values, and then calls a callback that sets
   * the state to the return values. It then asserts the state is correct.
   */
  @Test
  void testCallbackReturn8() {
    WasmInvoker invoker =
        WasmInvokerImpl.fromWasmBytes(load("contract_return_data_caller-8-0-0.wasm"), CONFIG_V8);
    var context = new TestSharedContext();
    WasmInvoker.WasmResult init = invoker.executeWasm(10000, "init", context, null, null);
    byte[] initState = init.state().getState();

    var executionResultU64 =
        CallbackContext.createResult(
            context.getOriginalTransactionHash(),
            true,
            SafeDataInputStream.createFromBytes(new byte[] {0, 0, 0, 0, 0, 0, 0, 42}));
    var executionResultString =
        CallbackContext.createResult(
            context.getOriginalTransactionHash(),
            true,
            SafeDataInputStream.createFromBytes(
                SafeDataOutputStream.serialize(writer -> writer.writeString("Hello"))));
    var executionResultBool =
        CallbackContext.createResult(
            context.getOriginalTransactionHash(),
            true,
            SafeDataInputStream.createFromBytes(new byte[] {1}));
    var callbackContext =
        CallbackContext.create(
            FixedList.create(
                List.of(executionResultU64, executionResultString, executionResultBool)));
    WasmInvoker.WasmResult callback =
        invoker.executeWasm(10000, "callback_10", context, callbackContext, null, initState);
    var callbackState =
        new LittleEndianByteInput(new ByteArrayInputStream(callback.state().getState()));
    assertThat(callbackState.readU64()).isEqualTo(42);
    assertThat(callbackState.readString()).isEqualTo("Hello");
    assertThat(callbackState.readBoolean()).isEqualTo(true);
  }

  static byte[] load() {
    return load("wasm-invoker-test.wasm");
  }

  static byte[] load(String name) {
    try (InputStream stream = WasmInvokerImplTest.class.getResourceAsStream(name)) {
      return Objects.requireNonNull(stream).readAllBytes();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Test
  void convertUsedInstructions() {
    assertThat(WasmInvokerImpl.convertToGas(WasmInvoker.INSTRUCTION_CYCLES_PER_GAS)).isEqualTo(1);
    assertThat(WasmInvokerImpl.convertToGas(WasmInvoker.INSTRUCTION_CYCLES_PER_GAS - 1))
        .isEqualTo(1);
    assertThat(WasmInvokerImpl.convertToGas(WasmInvoker.INSTRUCTION_CYCLES_PER_GAS + 1))
        .isEqualTo(2);
  }

  @Test
  void testExternalAvl() {
    WasmInvoker invoker =
        WasmInvokerImpl.fromWasmBytes(load("wasm-external-avl-tree.wasm"), CONFIG_V8);
    var context = new TestSharedContext();
    byte[] initRpc = new byte[0];
    WasmInvoker.WasmResult init = invoker.executeWasm(10000, "init", context, null, null, initRpc);
    AvlTree<Integer, AvlTreeWrapper> avlTrees = init.state().getAvlTrees();
    assertThat(avlTrees.getValue(0)).isNotNull();
    assertThat(avlTrees.getValue(1)).isNull();

    byte[] key = HexFormat.of().parseHex("010000000000000000000000000000000000000003");
    byte[] value = HexFormat.of().parseHex("42000000000000000000000000000000");
    // insert action
    WasmInvoker.WasmResult insert =
        invoker.executeWasm(
            10000,
            "action_01",
            context,
            null,
            new AvlTrees(avlTrees),
            init.state().getState(),
            key,
            Arrays.reverse(value));
    AvlTreeWrapper tree = insert.state().getAvlTrees().getValue(0);
    assertThat(tree.getValue(key)).containsExactly(value);

    // Expect result field in state to contain None
    assertThat(insert.state().getState()[4]).isEqualTo((byte) 0);
    // insert action
    WasmInvoker.WasmResult get =
        invoker.executeWasm(
            10000,
            "action_03",
            context,
            null,
            new AvlTrees(insert.state().getAvlTrees()),
            insert.state().getState(),
            key);
    tree = get.state().getAvlTrees().getValue(0);
    // No change to avl trees
    assertThat(tree.getValue(key)).containsExactly(value);
    // Expect result field in state to contain Some(value)
    assertThat(get.state().getState()[4]).isEqualTo((byte) 1);
    assertThat(get.state().getState()).endsWith(value);

    // remove action
    WasmInvoker.WasmResult remove =
        invoker.executeWasm(
            10000,
            "action_02",
            context,
            null,
            new AvlTrees(get.state().getAvlTrees()),
            get.state().getState(),
            key);
    tree = remove.state().getAvlTrees().getValue(0);
    assertThat(tree.getValue(key)).isNull();
  }
}
