package com.partisiablockchain.language.wasminvoker;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.SharedContext;
import com.partisiablockchain.crypto.Hash;

final class TestSharedContext implements SharedContext {

  TestSharedContext() {}

  @Override
  public BlockchainAddress getContractAddress() {
    return BlockchainAddress.fromString("010000000000000000000000000000000000000000");
  }

  @Override
  public long getBlockTime() {
    return 0;
  }

  @Override
  public long getBlockProductionTime() {
    return 0;
  }

  @Override
  public BlockchainAddress getFrom() {
    return BlockchainAddress.fromString("000000000000000000000000000000000000000000");
  }

  @Override
  public Hash getCurrentTransactionHash() {
    return Hash.fromString("0000000000000000000000000000000000000000000000000000000000000001");
  }

  @Override
  public Hash getOriginalTransactionHash() {
    return Hash.fromString("0000000000000000000000000000000000000000000000000000000000000002");
  }
}
