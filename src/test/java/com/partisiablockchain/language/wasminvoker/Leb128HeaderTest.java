package com.partisiablockchain.language.wasminvoker;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.secata.stream.SafeDataInputStream;
import org.junit.jupiter.api.Test;

final class Leb128HeaderTest {

  @Test
  void invalid() {
    assertThatThrownBy(() -> assertLength(0, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF))
        .hasMessageContaining("32-bit LEB128");
  }

  @Test
  void valid() {
    assertLength(1, 0);
    assertLength(1, 1);
    assertLength(1, 65);
    assertLength(1, 127);

    assertLength(2, 128, 1);
    assertLength(2, 192, 1);
    assertLength(2, 255, 1);
    assertLength(2, 128, 2);

    assertLength(3, 0xE5, 0x8E, 0x26);

    assertLength(4, 0xE5, 0x8E, 0x26 | 0x80, 0x26);

    assertLength(5, 0xFF, 0xFF, 0xFF, 0xFF, 0x0F);
  }

  void assertLength(int expectedLength, int... values) {
    var bytes = new byte[values.length];
    for (int i = 0; i < values.length; i++) {
      bytes[i] = (byte) values[i];
    }

    var readLength = Leb128Header.readHeaderLength(bytes);
    assertThat(readLength).isEqualTo(expectedLength);
  }

  @Test
  void invalidRead() {
    assertThatThrownBy(() -> assertRead(0xFF, 0xFF, 0xFF, 0xFF, 0xFF))
        .hasMessageContaining("32-bit LEB128");
    assertThatThrownBy(() -> assertRead(0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF))
        .hasMessageContaining("32-bit LEB128");
  }

  @Test
  void validRead() {
    assertRead(0);
    assertRead(1);
    assertRead(65);
    assertRead(127);

    assertRead(0x80, 1);
    assertRead(0x80 | 0x40, 1);
    assertRead(0x80 | 0xff, 1);
    assertRead(0x80, 2);

    assertRead(0xE5, 0x8E, 0x26);

    assertRead(0xE5, 0x8E, 0x26 | 0x80, 0x26);

    assertRead(0xFF, 0xFF, 0xFF, 0xFF, 0x0F);
  }

  private static void assertRead(int... values) {
    var bytes = new byte[values.length];
    for (int i = 0; i < values.length; i++) {
      bytes[i] = (byte) values[i];
    }

    var readBytes =
        SafeDataInputStream.readFully(bytes, stream -> Leb128Header.readHeaderBytes(stream));
    assertThat(readBytes).isEqualTo(bytes);
  }
}
