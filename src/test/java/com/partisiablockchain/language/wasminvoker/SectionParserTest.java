package com.partisiablockchain.language.wasminvoker;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.Test;

/** Tests for WasmRealContract. */
public final class SectionParserTest {

  @Test
  public void happyCase() {
    final byte[] data =
        Hex.decode(
            "09 00 00 00 05" // Section 9: 5 bytes
                + "50 51 52 53 54" // Section 9 data
                + "11 00 00 00 00" // Section 17: 0 bytes
            );

    assertThat(SectionParser.parseSections(data))
        .hasSize(2)
        .containsEntry((byte) 0x09, new byte[] {0x50, 0x51, 0x52, 0x53, 0x54})
        .containsEntry((byte) 0x11, new byte[0]);
  }

  @Test
  public void happyCase2() {
    final byte[] data =
        Hex.decode(
            "00 00 00 00 01 00" // Section 0: 1 byte: 0
                + "01 00 00 00 01 01" // Section 1: 1 byte: 1
                + "02 00 00 00 01 02" // Section 2: 1 byte: 2
                + "03 00 00 00 01 03" // Section 3: 1 byte: 3
                + "04 00 00 00 01 04" // Section 4: 1 byte: 4
            );

    assertThat(SectionParser.parseSections(data))
        .hasSize(5)
        .containsEntry((byte) 0x00, new byte[] {0x0})
        .containsEntry((byte) 0x01, new byte[] {0x1})
        .containsEntry((byte) 0x02, new byte[] {0x2})
        .containsEntry((byte) 0x03, new byte[] {0x3})
        .containsEntry((byte) 0x04, new byte[] {0x4});
  }

  @Test
  public void invalidOrder() {
    final byte[] data =
        Hex.decode(
            "11 00 00 00 00" // Section 17: 0 bytes
                + "09 00 00 00 00" // Section 9: 0 bytes
            );

    assertThatThrownBy(() -> SectionParser.parseSections(data))
        .isInstanceOf(RuntimeException.class)
        .hasMessage(
            "Invalid section 9: Duplicated or incorrectly ordered. Expected section with id of at"
                + " least 18");
  }

  @Test
  public void invalidDuplicated() {
    final byte[] data =
        Hex.decode(
            "09 00 00 00 00" // Section 9: 0 bytes
                + "09 00 00 00 00" // Section 9: 0 bytes
            );

    assertThatThrownBy(() -> SectionParser.parseSections(data))
        .isInstanceOf(RuntimeException.class)
        .hasMessage(
            "Invalid section 9: Duplicated or incorrectly ordered. Expected section with id of at"
                + " least 10");
  }

  @Test
  public void invalidLengthIsNegative() {
    final byte[] data =
        Hex.decode(
            "11 F0 00 00 00" // Section 17: negative bytes
            );

    assertThatThrownBy(() -> SectionParser.parseSections(data))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Invalid section 17: Section length -268435456 is negative");
  }

  @Test
  public void invalidLengthLargerThanBuffer() {
    final byte[] data =
        Hex.decode(
            "11 00 00 01 00" // Section 17: 256 bytes, but no data
            );

    assertThatThrownBy(() -> SectionParser.parseSections(data))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Invalid section 17: Section length 256 longer than remaining data");
  }

  @Test
  public void invalidLengthLargerThanRemaining() {
    final byte[] data =
        Hex.decode(
            "11 00 00 00 02 01" // Section 17: len 2, but only 1 byte of data
            );

    assertThatThrownBy(() -> SectionParser.parseSections(data))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Invalid section 17: Section length 2 longer than remaining data");
  }
}
