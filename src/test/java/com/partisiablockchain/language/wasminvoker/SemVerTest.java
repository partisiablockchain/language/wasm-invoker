package com.partisiablockchain.language.wasminvoker;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import org.junit.jupiter.api.Test;

final class SemVerTest {

  @SuppressWarnings("ConstantConditions")
  @Test
  void compatibleWith() {

    assertIncompatible(new SemVer(1, 0, 0), new SemVer(0, 0, 0));
    assertIncompatible(new SemVer(2, 0, 0), new SemVer(1, 0, 0));

    for (int i = 0; i < 256; i++) {
      assertIncompatible(new SemVer(2, 0, 0), new SemVer(1, i, 0));
      assertIncompatible(new SemVer(2, 0, 0), new SemVer(1, 0, i));
      assertIncompatible(new SemVer(2, 0, 0), new SemVer(1, i, i));

      assertCompatible(new SemVer(i, 0, 0), new SemVer(i, 0, 0));
      assertCompatible(new SemVer(1, i, 0), new SemVer(1, i, 0));
      assertCompatible(new SemVer(1, 0, i), new SemVer(1, 0, i));
      assertCompatible(new SemVer(1, 0, i), new SemVer(1, 0, 255 - i));
      assertCompatible(new SemVer(2, i, 0), new SemVer(2, 0, 0));
    }

    for (int i = 1; i < 256; i++) {
      assertIncompatible(new SemVer(2, 0, 0), new SemVer(2, i, 0));
    }
  }

  private void assertCompatible(final SemVer binderVersion, final SemVer programVersion) {
    assertThat(Configuration.isVersionSupportedByBinderWithVersion(programVersion, binderVersion))
        .isTrue();
  }

  private void assertIncompatible(final SemVer binderVersion, final SemVer programVersion) {
    assertThat(Configuration.isVersionSupportedByBinderWithVersion(programVersion, binderVersion))
        .isFalse();
  }

  @Test
  void parseInvalidVersions() {
    assertThat(parse(-1, 1, 1)).isNull();
    assertThat(parse(1, -1, 1)).isNull();
    assertThat(parse(1, 1, -1)).isNull();

    assertThatThrownBy(() -> parse(256, 255, 255)).hasMessageContaining("256");
    assertThatThrownBy(() -> parse(255, 256, 255)).hasMessageContaining("256");
    assertThatThrownBy(() -> parse(255, 255, 256)).hasMessageContaining("256");
  }

  @Test
  void parseValidVersions() {
    for (int patch = 0; patch < 256; patch++) {
      assertParsed(0, 0, patch);
    }

    for (int minor = 0; minor < 256; minor++) {
      assertParsed(0, minor, 0);
    }

    for (int major = 0; major < 256; major++) {
      assertParsed(major, 0, 0);
    }

    assertParsed(0, 0, 0);
    assertParsed(1, 2, 43);
    assertParsed(255, 255, 255);
  }

  private static void assertParsed(int major, int minor, int patch) {
    var version = parse(major, minor, patch);
    assertThat(version.major()).isEqualTo(major);
    assertThat(version.minor()).isEqualTo(minor);
    assertThat(version.patch()).isEqualTo(patch);
  }

  private static SemVer parse(int major, int minor, int patch) {
    return Configuration.parseWasmExportToString(
        String.format("__PBC_VERSION_BINDER_%d_%d_%d", major, minor, patch));
  }
}
