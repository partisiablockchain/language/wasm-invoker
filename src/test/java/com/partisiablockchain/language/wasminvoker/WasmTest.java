package com.partisiablockchain.language.wasminvoker;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.wasm.common.WasmModule;
import com.partisiablockchain.language.wasm.common.literal.Int32;
import com.partisiablockchain.language.wasm.common.literal.Int64;
import com.partisiablockchain.language.wasm.common.literal.Literal;
import com.partisiablockchain.language.wasm.interpreter.WasmInstance;
import com.partisiablockchain.language.wasm.parser.WasmParser;
import java.io.IOException;
import java.util.List;

/** Abstract implementation for WASM tests. */
public abstract class WasmTest {

  protected WasmInstance instance;
  protected byte[] bytes;
  protected WasmModule module;

  /**
   * Run a wasm function returning void.
   *
   * @param name name of the function
   * @param arguments arguments of the function
   */
  protected void runVoid(String name, Literal... arguments) {
    runFunction(name, arguments);
  }

  /**
   * Run a wasm function returning an I32.
   *
   * @param name name of the function
   * @param arguments arguments of the function
   * @return the return value of the function
   */
  protected int runI32(String name, Literal... arguments) {
    Int32 literal = (Int32) runFunction(name, arguments);
    return literal.value();
  }

  /**
   * Run a wasm function returning an I64.
   *
   * @param name name of the function
   * @param arguments arguments of the function
   * @return the return value of the function
   */
  protected long runI64(String name, Literal... arguments) {
    Int64 literal = (Int64) runFunction(name, arguments);
    return literal.value();
  }

  /**
   * Run a wasm function returning some value.
   *
   * @param name name of the function
   * @param arguments arguments of the function
   * @return the return value of the function
   */
  protected Literal runFunction(String name, Literal... arguments) {
    return runFunction(name, Long.MAX_VALUE, arguments);
  }

  /**
   * Run a wasm function returning some value.
   *
   * @param name name of the function
   * @param maxCycles the maximum number of cycles the function can use
   * @param arguments arguments of the function
   * @return the return value of the function
   */
  protected Literal runFunction(String name, long maxCycles, Literal... arguments) {
    List<Literal> list = instance.runFunction(name, maxCycles, List.of(arguments));
    if (list.isEmpty()) {
      return null;
    } else {
      return list.get(0);
    }
  }

  /**
   * Run a wasm function returning some value.
   *
   * @param name name of the function
   * @param maxCyclesForThisCall the precise number of cycles this function uses
   * @param arguments arguments of the function
   * @return the return value of the function
   */
  protected Literal runFunctionWithPreciseCycles(
      final String name, final long maxCyclesForThisCall, final Literal... arguments) {
    final Literal result = runFunction(name, maxCyclesForThisCall, arguments);
    assertThat(instance.getCyclesUsed())
        .describedAs("runFunctionWithPreciseCycles requires a precise amount of cycles usage")
        .isEqualTo(maxCyclesForThisCall);
    return result;
  }

  /**
   * Load a wat program from a file to a wasm instance.
   *
   * @param path test class
   * @param name name of the wat file
   */
  protected void load(Class<?> path, String name) throws IOException {
    this.bytes = Wat2Wasm.translate(path.getResourceAsStream(name), WasmTest.class);
    this.module = new WasmParser(bytes).parse();
    this.instance = WasmInstance.forModule(module);
  }

  /**
   * Parse a wat program to a wasm instance.
   *
   * @param path test class
   * @param watProgram the wat program
   */
  protected void parse(Class<?> path, String watProgram) {
    this.bytes = Wat2Wasm.translate(watProgram, path);
    this.module = new WasmParser(bytes).parse();
    instance = WasmInstance.forModule(module);
  }
}
