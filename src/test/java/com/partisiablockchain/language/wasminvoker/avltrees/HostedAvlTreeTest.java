package com.partisiablockchain.language.wasminvoker.avltrees;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.wasm.common.TrapException;
import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.literal.Int32;
import com.partisiablockchain.language.wasminvoker.WasmTest;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.tree.AvlTree;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

final class HostedAvlTreeTest extends WasmTest {

  static final long CALL_OVERHEAD = 2L;

  private void setupInstance() {
    // Setup
    parse(
        HostedAvlTreeTest.class,
        """
        (module
            (import "pbc" "avl_new" (func (result i32)))
            (import "pbc" "avl_insert" (func (param i32 i32 i32 i32 i32)))
            (import "pbc" "avl_get_size" (func (param i32 i32 i32) (result i32)))
            (import "pbc" "avl_get" (func (param i32 i32 i32 i32) (result i32)))
            (import "pbc" "avl_remove" (func (param i32 i32 i32)))
            (import "pbc" "avl_len" (func (param i32) (result i32)))
            (import "pbc" "avl_get_next_size" (func (param i32 i32 i32) (result i32)))
            (import "pbc" "avl_get_next" (func (param i32 i32 i32 i32) (result i32)))
            (export "avl_new" (func 0))
            (export "avl_insert" (func 1))
            (export "avl_get_size" (func 2))
            (export "avl_get" (func 3))
            (export "avl_remove" (func 4))
            (export "avl_len" (func 5))
            (export "avl_get_next_size" (func 6))
            (export "avl_get_next" (func 7))
            (memory 1)
            (data 0 (i32.const 16) "Hello, World!")
            (data 0 (i32.const 160) "Key")
            (data 0 (i32.const 200) "Key2")
        )
        """);
  }

  @Test
  void newTree() {
    HostedAvlTree handler = new HostedAvlTree(AvlTree.create());
    int treeId = handler.avlNew();
    int treeId2 = handler.avlNew();
    assertThat(treeId).isEqualTo(0);
    assertThat(treeId2).isEqualTo(1);
  }

  @Test
  void insertTree() {
    HostedAvlTree handler = new HostedAvlTree(AvlTree.create());
    int treeId = handler.avlNew();
    byte[] key = new byte[] {1, 2};
    byte[] value = new byte[] {3, 4};

    handler.avlInsert(treeId, key, value);

    byte[] receivedValue = handler.avlGet(treeId, key);
    assertThat(receivedValue).containsExactly(value);

    handler.avlRemove(treeId, key);
    byte[] nullValue = handler.avlGet(treeId, key);

    assertThat(nullValue).isNull();
  }

  @Test
  void getNext() {
    HostedAvlTree handler = new HostedAvlTree(AvlTree.create());
    int treeId = handler.avlNew();
    byte[] key1 = new byte[] {1, 2};
    byte[] value1 = new byte[] {3, 4};
    byte[] key2 = new byte[] {5, 6};
    byte[] value2 = new byte[] {7, 8};

    handler.avlInsert(treeId, key1, value1);
    handler.avlInsert(treeId, key2, value2);

    Map.Entry<byte[], byte[]> entry1 = handler.avlGetNext(treeId, null);
    assertThat(entry1.getKey()).containsExactly(key1);
    assertThat(entry1.getValue()).containsExactly(value1);
    assertThat(handler.avlGetNextEntrySize(treeId, null))
        .isEqualTo(entry1.getKey().length + entry1.getValue().length);

    Map.Entry<byte[], byte[]> entry2 = handler.avlGetNext(treeId, key1);
    assertThat(entry2.getKey()).containsExactly(key2);
    assertThat(entry2.getValue()).containsExactly(value2);
    assertThat(handler.avlGetNextEntrySize(treeId, key1))
        .isEqualTo(entry2.getKey().length + entry2.getValue().length);

    Map.Entry<byte[], byte[]> entryNull = handler.avlGetNext(treeId, key2);
    assertThat(entryNull).isNull();
    assertThat(handler.avlGetNextEntrySize(treeId, key2)).isEqualTo(-1);
  }

  @Test
  void getReadOnly() {
    byte[] key1 = {1};
    AvlTree<ComparableByteArray, LargeByteArray> readOnly =
        AvlTree.create(Map.of(new ComparableByteArray(key1), new LargeByteArray(key1)));
    HostedAvlTree handler =
        new HostedAvlTree(
            AvlTree.create(), AvlTree.create(Map.of(-1, new AvlTreeWrapper(readOnly))));
    byte[] key2 = {2};
    assertThat(handler.avlGet(-1, key1)).containsExactly(key1);
    assertThat(handler.avlGetValueSize(-1, key1)).isEqualTo(key1.length);
    assertThat(handler.avlGet(-1, key2)).isNull();
    assertThat(handler.avlGetValueSize(-1, key2)).isEqualTo(-1);
  }

  @Test
  void sizeReadOnly() {
    byte[] key1 = {1};
    AvlTree<ComparableByteArray, LargeByteArray> readOnly =
        AvlTree.create(Map.of(new ComparableByteArray(key1), new LargeByteArray(key1)));
    HostedAvlTree handler =
        new HostedAvlTree(
            AvlTree.create(), AvlTree.create(Map.of(-1, new AvlTreeWrapper(readOnly))));
    assertThat(handler.avlSize(-1)).isEqualTo(1);
  }

  @Test
  void insertReadOnly() {
    byte[] key1 = {1};
    AvlTree<ComparableByteArray, LargeByteArray> readOnly =
        AvlTree.create(Map.of(new ComparableByteArray(key1), new LargeByteArray(key1)));
    HostedAvlTree handler =
        new HostedAvlTree(
            AvlTree.create(), AvlTree.create(Map.of(-1, new AvlTreeWrapper(readOnly))));
    Assertions.assertThatThrownBy(() -> handler.avlInsert(-1, new byte[] {0}, new byte[] {1}))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Can't modify read only avl trees");
    assertThat(handler.avlSize(-1)).isEqualTo(1);
  }

  @Test
  void removeReadOnly() {
    byte[] key1 = {1};
    AvlTree<ComparableByteArray, LargeByteArray> readOnly =
        AvlTree.create(Map.of(new ComparableByteArray(key1), new LargeByteArray(key1)));
    HostedAvlTree handler =
        new HostedAvlTree(
            AvlTree.create(), AvlTree.create(Map.of(-1, new AvlTreeWrapper(readOnly))));
    Assertions.assertThatThrownBy(() -> handler.avlRemove(-1, new byte[] {0}))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Can't modify read only avl trees");
    assertThat(handler.avlSize(-1)).isEqualTo(1);
  }

  @Test
  void getNextReadOnly() {
    byte[] key1 = {1};
    AvlTree<ComparableByteArray, LargeByteArray> readOnly =
        AvlTree.create(Map.of(new ComparableByteArray(key1), new LargeByteArray(key1)));
    HostedAvlTree handler =
        new HostedAvlTree(
            AvlTree.create(), AvlTree.create(Map.of(-1, new AvlTreeWrapper(readOnly))));
    Map.Entry<byte[], byte[]> first = handler.avlGetNext(-1, null);
    assertThat(first.getKey()).containsExactly(key1);
    assertThat(first.getValue()).containsExactly(key1);
    assertThat(handler.avlGetNextEntrySize(-1, null))
        .isEqualTo(first.getKey().length + first.getValue().length);
    Map.Entry<byte[], byte[]> second = handler.avlGetNext(-1, key1);
    assertThat(second).isNull();
    assertThat(handler.avlGetNextEntrySize(-1, key1)).isEqualTo(-1);
  }

  @Test
  void callAvlNew() {
    setupInstance();
    HostedAvlTree handler = new HostedAvlTree(AvlTree.create());
    handler.registerAvlFunctions(instance);
    Int32 rawTreeId =
        (Int32)
            runFunctionWithPreciseCycles(
                "avl_new", CALL_OVERHEAD + HostedAvlTree.INSTRUCTION_CYCLES_PER_CALL.asInt());
    int treeId = rawTreeId.value();
    assertThat(treeId).isEqualTo(0);
    assertThat(handler.getAvlTrees().getValue(treeId)).isNotNull();

    int treeId2 = runI32("avl_new");
    assertThat(treeId2).isEqualTo(1);
    assertThat(handler.getAvlTrees().getValue(treeId2)).isNotNull();
  }

  @Test
  void callAvlInsert() {
    setupInstance();
    HostedAvlTree handler = new HostedAvlTree(AvlTree.create());
    handler.registerAvlFunctions(instance);
    int treeId = handler.avlNew();
    runFunctionWithPreciseCycles(
        "avl_insert",
        CALL_OVERHEAD + HostedAvlTree.INSTRUCTION_CYCLES_PER_CALL.asInt(),
        new Int32(treeId),
        new Int32(160),
        new Int32(3),
        new Int32(16),
        new Int32(13));
    assertThat(handler.avlGet(treeId, "Key".getBytes(StandardCharsets.UTF_8)))
        .asString()
        .isEqualTo("Hello, World!");
  }

  @Test
  void callAvlRemove() {
    setupInstance();
    HostedAvlTree handler = new HostedAvlTree(AvlTree.create());
    handler.registerAvlFunctions(instance);
    int treeId = handler.avlNew();
    handler.avlInsert(
        treeId,
        "Key".getBytes(StandardCharsets.UTF_8),
        "Hello, World!".getBytes(StandardCharsets.UTF_8));

    assertThat(handler.avlGet(treeId, "Key".getBytes(StandardCharsets.UTF_8)))
        .asString()
        .isEqualTo("Hello, World!");

    runFunctionWithPreciseCycles(
        "avl_remove",
        CALL_OVERHEAD + HostedAvlTree.INSTRUCTION_CYCLES_PER_CALL.asInt(),
        new Int32(treeId),
        new Int32(160),
        new Int32(3));

    assertThat(handler.avlGet(treeId, "Key".getBytes(StandardCharsets.UTF_8))).isNull();
  }

  @Test
  void callAvlGetSize() {
    setupInstance();
    HostedAvlTree handler = new HostedAvlTree(AvlTree.create());
    handler.registerAvlFunctions(instance);
    int treeId = handler.avlNew();
    handler.avlInsert(
        treeId,
        "Key".getBytes(StandardCharsets.UTF_8),
        "Hello, World!".getBytes(StandardCharsets.UTF_8));

    Int32 rawSize =
        (Int32)
            runFunctionWithPreciseCycles(
                "avl_get_size",
                CALL_OVERHEAD + HostedAvlTree.INSTRUCTION_CYCLES_PER_CALL.asInt(),
                new Int32(treeId),
                new Int32(160),
                new Int32(3));
    int size = rawSize.value();

    assertThat(size).isEqualTo(13);

    int size0 = runI32("avl_get_size", new Int32(treeId), new Int32(160), new Int32(1));

    assertThat(size0).isEqualTo(-1);
  }

  @Test
  void callAvlGet() {
    setupInstance();
    HostedAvlTree handler = new HostedAvlTree(AvlTree.create());
    handler.registerAvlFunctions(instance);
    int treeId = handler.avlNew();
    handler.avlInsert(
        treeId,
        "Key".getBytes(StandardCharsets.UTF_8),
        "Hello, World!".getBytes(StandardCharsets.UTF_8));

    Int32 rawSuccess =
        (Int32)
            runFunctionWithPreciseCycles(
                "avl_get",
                CALL_OVERHEAD + HostedAvlTree.INSTRUCTION_CYCLES_PER_CALL.asInt(),
                new Int32(treeId),
                new Int32(160),
                new Int32(3),
                new Int32(400));
    int success = rawSuccess.value();

    assertThat(success).isEqualTo(1);

    assertThat(instance.getMemory(Uint31.ZERO).read(new Uint31(400), new Uint31(13)))
        .asString()
        .isEqualTo("Hello, World!");

    int fail = runI32("avl_get", new Int32(treeId), new Int32(160), new Int32(1), new Int32(400));

    assertThat(fail).isEqualTo(0);
  }

  @Test
  void callAvlLen() {
    setupInstance();
    HostedAvlTree handler = new HostedAvlTree(AvlTree.create());
    handler.registerAvlFunctions(instance);
    int treeId = handler.avlNew();
    handler.avlInsert(
        treeId,
        "Key".getBytes(StandardCharsets.UTF_8),
        "Hello, World!".getBytes(StandardCharsets.UTF_8));

    Int32 rawSize =
        (Int32)
            runFunctionWithPreciseCycles(
                "avl_len",
                CALL_OVERHEAD + HostedAvlTree.INSTRUCTION_CYCLES_PER_CALL.asInt(),
                new Int32(treeId));
    int size = rawSize.value();

    assertThat(size).isEqualTo(1);
  }

  @Test
  void callAvlGetNextSize() {
    setupInstance();
    HostedAvlTree handler = new HostedAvlTree(AvlTree.create());
    handler.registerAvlFunctions(instance);
    int treeId = handler.avlNew();
    handler.avlInsert(
        treeId,
        "Key".getBytes(StandardCharsets.UTF_8),
        "Hello, World!".getBytes(StandardCharsets.UTF_8));
    handler.avlInsert(
        treeId, "Key2".getBytes(StandardCharsets.UTF_8), "Test".getBytes(StandardCharsets.UTF_8));

    Int32 rawSize1 =
        (Int32)
            runFunctionWithPreciseCycles(
                "avl_get_next_size",
                CALL_OVERHEAD + HostedAvlTree.INSTRUCTION_CYCLES_PER_CALL.asInt(),
                new Int32(treeId),
                new Int32(-1),
                new Int32(-1));
    int size1 = rawSize1.value();

    assertThat(size1).isEqualTo(3 + 13);

    Int32 rawSize2 =
        (Int32)
            runFunctionWithPreciseCycles(
                "avl_get_next_size",
                CALL_OVERHEAD + HostedAvlTree.INSTRUCTION_CYCLES_PER_CALL.asInt(),
                new Int32(treeId),
                new Int32(160),
                new Int32(3));
    int size2 = rawSize2.value();

    assertThat(size2).isEqualTo(4 + 4);

    Int32 rawSize3 =
        (Int32)
            runFunctionWithPreciseCycles(
                "avl_get_next_size",
                CALL_OVERHEAD + HostedAvlTree.INSTRUCTION_CYCLES_PER_CALL.asInt(),
                new Int32(treeId),
                new Int32(200),
                new Int32(4));
    int size3 = rawSize3.value();

    assertThat(size3).isEqualTo(-1);

    Int32 rawSizeCoverage =
        (Int32)
            runFunctionWithPreciseCycles(
                "avl_get_next_size",
                CALL_OVERHEAD + HostedAvlTree.INSTRUCTION_CYCLES_PER_CALL.asInt(),
                new Int32(treeId),
                new Int32(160),
                new Int32(-1));
    int sizeCoverage = rawSizeCoverage.value();

    assertThat(sizeCoverage).isEqualTo(3 + 13);
  }

  @Test
  void callAvlGetNext() {
    setupInstance();
    HostedAvlTree handler = new HostedAvlTree(AvlTree.create());
    handler.registerAvlFunctions(instance);
    int treeId = handler.avlNew();
    handler.avlInsert(
        treeId,
        "Key".getBytes(StandardCharsets.UTF_8),
        "Hello, World!".getBytes(StandardCharsets.UTF_8));
    handler.avlInsert(
        treeId, "Key2".getBytes(StandardCharsets.UTF_8), "Test".getBytes(StandardCharsets.UTF_8));

    Int32 rawSuccess =
        (Int32)
            runFunctionWithPreciseCycles(
                "avl_get_next",
                CALL_OVERHEAD + HostedAvlTree.INSTRUCTION_CYCLES_PER_CALL.asInt(),
                new Int32(treeId),
                new Int32(-1),
                new Int32(-1),
                new Int32(400));
    int success = rawSuccess.value();

    assertThat(success).isEqualTo(1);

    assertThat(instance.getMemory(Uint31.ZERO).read(new Uint31(400), new Uint31(3 + 13)))
        .asString()
        .isEqualTo("KeyHello, World!");

    Int32 rawSuccess2 =
        (Int32)
            runFunctionWithPreciseCycles(
                "avl_get_next",
                CALL_OVERHEAD + HostedAvlTree.INSTRUCTION_CYCLES_PER_CALL.asInt(),
                new Int32(treeId),
                new Int32(160),
                new Int32(3),
                new Int32(400));
    int success2 = rawSuccess2.value();

    assertThat(success2).isEqualTo(1);

    assertThat(instance.getMemory(Uint31.ZERO).read(new Uint31(400), new Uint31(4 + 4)))
        .asString()
        .isEqualTo("Key2Test");

    int end =
        runI32("avl_get_next", new Int32(treeId), new Int32(200), new Int32(4), new Int32(400));

    assertThat(end).isEqualTo(0);

    Int32 rawSuccessCoverage =
        (Int32)
            runFunctionWithPreciseCycles(
                "avl_get_next",
                CALL_OVERHEAD + HostedAvlTree.INSTRUCTION_CYCLES_PER_CALL.asInt(),
                new Int32(treeId),
                new Int32(0),
                new Int32(-1),
                new Int32(400));
    int successCoverage = rawSuccessCoverage.value();

    assertThat(successCoverage).isEqualTo(1);

    assertThat(instance.getMemory(Uint31.ZERO).read(new Uint31(400), new Uint31(3 + 13)))
        .asString()
        .isEqualTo("KeyHello, World!");
  }

  @Test
  void badMemoryAccess() {
    setupInstance();
    HostedAvlTree hostedAvlTree = new HostedAvlTree(AvlTree.create());
    hostedAvlTree.registerAvlFunctions(instance);

    final int memorySize = instance.getMemory(Uint31.ZERO).size().asInt();

    Assertions.assertThatThrownBy(
            () ->
                runFunction(
                    "avl_insert",
                    new Int32(0),
                    new Int32(-5),
                    new Int32(3),
                    new Int32(160),
                    new Int32(13)))
        .isInstanceOf(TrapException.class)
        .hasMessage(
            "Trap: Out of bounds memory access. "
                + "Reading avl_insert key 3 bytes at address -5, buffer len: 65536\n"
                + "    at hosted:avl_insert():0\n"
                + "    at call_avl_insert():1");

    Assertions.assertThatThrownBy(
            () ->
                runFunction(
                    "avl_insert",
                    new Int32(0),
                    new Int32(16),
                    new Int32(-5),
                    new Int32(160),
                    new Int32(13)))
        .isInstanceOf(TrapException.class)
        .hasMessage(
            "Trap: Out of bounds memory access. "
                + "Reading avl_insert key -5 bytes at address 16, buffer len: 65536\n"
                + "    at hosted:avl_insert():0\n"
                + "    at call_avl_insert():1");

    Assertions.assertThatThrownBy(
            () ->
                runFunction(
                    "avl_insert",
                    new Int32(0),
                    new Int32(memorySize),
                    new Int32(3),
                    new Int32(160),
                    new Int32(13)))
        .isInstanceOf(TrapException.class)
        .hasMessage(
            "Trap: Out of bounds memory access. "
                + "Reading avl_insert key 3 bytes at address 65536, buffer len: 65536\n"
                + "    at hosted:avl_insert():0\n"
                + "    at call_avl_insert():1");

    Assertions.assertThatThrownBy(
            () ->
                runFunction(
                    "avl_insert",
                    new Int32(0),
                    new Int32(16),
                    new Int32(3),
                    new Int32(-5),
                    new Int32(13)))
        .isInstanceOf(TrapException.class)
        .hasMessage(
            "Trap: Out of bounds memory access. "
                + "Reading avl_insert value 13 bytes at address -5, buffer len: 65536\n"
                + "    at hosted:avl_insert():0\n"
                + "    at call_avl_insert():1");

    Assertions.assertThatThrownBy(
            () ->
                runFunction(
                    "avl_insert",
                    new Int32(0),
                    new Int32(16),
                    new Int32(3),
                    new Int32(160),
                    new Int32(-5)))
        .isInstanceOf(TrapException.class)
        .hasMessage(
            "Trap: Out of bounds memory access. "
                + "Reading avl_insert value -5 bytes at address 160, buffer len: 65536\n"
                + "    at hosted:avl_insert():0\n"
                + "    at call_avl_insert():1");

    Assertions.assertThatThrownBy(
            () ->
                runFunction(
                    "avl_insert",
                    new Int32(0),
                    new Int32(16),
                    new Int32(3),
                    new Int32(memorySize),
                    new Int32(13)))
        .isInstanceOf(TrapException.class)
        .hasMessage(
            "Trap: Out of bounds memory access. "
                + "Reading avl_insert value 13 bytes at address 65536, buffer len: 65536\n"
                + "    at hosted:avl_insert():0\n"
                + "    at call_avl_insert():1");

    Assertions.assertThatThrownBy(
            () -> runFunction("avl_get_size", new Int32(0), new Int32(-5), new Int32(3)))
        .isInstanceOf(TrapException.class)
        .hasMessage(
            "Trap: Out of bounds memory access. "
                + "Reading avl_get_size key 3 bytes at address -5, buffer len: 65536\n"
                + "    at hosted:avl_get_size():0\n"
                + "    at call_avl_get_size():1");

    Assertions.assertThatThrownBy(
            () -> runFunction("avl_get_size", new Int32(0), new Int32(16), new Int32(-5)))
        .isInstanceOf(TrapException.class)
        .hasMessage(
            "Trap: Out of bounds memory access. "
                + "Reading avl_get_size key -5 bytes at address 16, buffer len: 65536\n"
                + "    at hosted:avl_get_size():0\n"
                + "    at call_avl_get_size():1");

    Assertions.assertThatThrownBy(
            () -> runFunction("avl_get_size", new Int32(0), new Int32(memorySize), new Int32(3)))
        .isInstanceOf(TrapException.class)
        .hasMessage(
            "Trap: Out of bounds memory access. "
                + "Reading avl_get_size key 3 bytes at address 65536, buffer len: 65536\n"
                + "    at hosted:avl_get_size():0\n"
                + "    at call_avl_get_size():1");

    Assertions.assertThatThrownBy(
            () -> runFunction("avl_get", new Int32(0), new Int32(-5), new Int32(3), new Int32(0)))
        .isInstanceOf(TrapException.class)
        .hasMessage(
            "Trap: Out of bounds memory access. "
                + "Reading avl_get key 3 bytes at address -5, buffer len: 65536\n"
                + "    at hosted:avl_get():0\n"
                + "    at call_avl_get():1");

    Assertions.assertThatThrownBy(
            () -> runFunction("avl_get", new Int32(0), new Int32(16), new Int32(-5), new Int32(0)))
        .isInstanceOf(TrapException.class)
        .hasMessage(
            "Trap: Out of bounds memory access. "
                + "Reading avl_get key -5 bytes at address 16, buffer len: 65536\n"
                + "    at hosted:avl_get():0\n"
                + "    at call_avl_get():1");

    Assertions.assertThatThrownBy(
            () ->
                runFunction(
                    "avl_get", new Int32(0), new Int32(memorySize), new Int32(3), new Int32(0)))
        .isInstanceOf(TrapException.class)
        .hasMessage(
            "Trap: Out of bounds memory access. "
                + "Reading avl_get key 3 bytes at address 65536, buffer len: 65536\n"
                + "    at hosted:avl_get():0\n"
                + "    at call_avl_get():1");

    Assertions.assertThatThrownBy(
            () -> runFunction("avl_remove", new Int32(0), new Int32(-5), new Int32(3)))
        .isInstanceOf(TrapException.class)
        .hasMessage(
            "Trap: Out of bounds memory access. "
                + "Reading avl_remove key 3 bytes at address -5, buffer len: 65536\n"
                + "    at hosted:avl_remove():0\n"
                + "    at call_avl_remove():1");

    Assertions.assertThatThrownBy(
            () -> runFunction("avl_remove", new Int32(0), new Int32(16), new Int32(-5)))
        .isInstanceOf(TrapException.class)
        .hasMessage(
            "Trap: Out of bounds memory access. "
                + "Reading avl_remove key -5 bytes at address 16, buffer len: 65536\n"
                + "    at hosted:avl_remove():0\n"
                + "    at call_avl_remove():1");

    Assertions.assertThatThrownBy(
            () -> runFunction("avl_remove", new Int32(0), new Int32(memorySize), new Int32(3)))
        .isInstanceOf(TrapException.class)
        .hasMessage(
            "Trap: Out of bounds memory access. "
                + "Reading avl_remove key 3 bytes at address 65536, buffer len: 65536\n"
                + "    at hosted:avl_remove():0\n"
                + "    at call_avl_remove():1");

    hostedAvlTree.avlNew();
    hostedAvlTree.avlInsert(
        0, "Key".getBytes(StandardCharsets.UTF_8), "Value".getBytes(StandardCharsets.UTF_8));

    Assertions.assertThatThrownBy(
            () -> runFunction("avl_get", new Int32(0), new Int32(160), new Int32(3), new Int32(-5)))
        .isInstanceOf(TrapException.class)
        .hasMessage(
            "Trap: Out of bounds memory access. "
                + "Writing avl_get value 5 bytes at address -5, buffer len: 65536\n"
                + "    at hosted:avl_get():0\n"
                + "    at call_avl_get():1");

    Assertions.assertThatThrownBy(
            () ->
                runFunction(
                    "avl_get", new Int32(0), new Int32(160), new Int32(3), new Int32(memorySize)))
        .isInstanceOf(TrapException.class)
        .hasMessage(
            "Trap: Out of bounds memory access. "
                + "Writing avl_get value 5 bytes at address 65536, buffer len: 65536\n"
                + "    at hosted:avl_get():0\n"
                + "    at call_avl_get():1");

    Assertions.assertThatThrownBy(
            () ->
                runFunction("avl_get_next_size", new Int32(0), new Int32(memorySize), new Int32(3)))
        .isInstanceOf(TrapException.class)
        .hasMessage(
            "Trap: Out of bounds memory access. "
                + "Reading avl_get_next_size key 3 bytes at address 65536, buffer len: 65536\n"
                + "    at hosted:avl_get_next_size():0\n"
                + "    at call_avl_get_next_size():1");

    Assertions.assertThatThrownBy(
            () ->
                runFunction(
                    "avl_get_next",
                    new Int32(0),
                    new Int32(memorySize),
                    new Int32(3),
                    new Int32(0)))
        .isInstanceOf(TrapException.class)
        .hasMessage(
            "Trap: Out of bounds memory access. "
                + "Reading avl_get_next key 3 bytes at address 65536, buffer len: 65536\n"
                + "    at hosted:avl_get_next():0\n"
                + "    at call_avl_get_next():1");

    Assertions.assertThatThrownBy(
            () ->
                runFunction(
                    "avl_get_next", new Int32(0), new Int32(160), new Int32(-1), new Int32(-5)))
        .isInstanceOf(TrapException.class)
        .hasMessage(
            "Trap: Out of bounds memory access. "
                + "Writing avl_get_next value 8 bytes at address -5, buffer len: 65536\n"
                + "    at hosted:avl_get_next():0\n"
                + "    at call_avl_get_next():1");

    Assertions.assertThatThrownBy(
            () ->
                runFunction(
                    "avl_get_next",
                    new Int32(0),
                    new Int32(160),
                    new Int32(-1),
                    new Int32(memorySize)))
        .isInstanceOf(TrapException.class)
        .hasMessage(
            "Trap: Out of bounds memory access. "
                + "Writing avl_get_next value 8 bytes at address 65536, buffer len: 65536\n"
                + "    at hosted:avl_get_next():0\n"
                + "    at call_avl_get_next():1");
  }
}
