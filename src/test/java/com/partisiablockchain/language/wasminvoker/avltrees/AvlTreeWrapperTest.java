package com.partisiablockchain.language.wasminvoker.avltrees;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.tree.AvlTree;
import org.junit.jupiter.api.Test;

final class AvlTreeWrapperTest {

  @Test
  void getAvlTree() {
    AvlTreeWrapper avlTreeWrapper = AvlTreeWrapper.create();
    ComparableByteArray key = new ComparableByteArray(new byte[] {1});
    avlTreeWrapper = avlTreeWrapper.set(key.getData(), new byte[] {2});
    AvlTree<ComparableByteArray, LargeByteArray> avlTree = avlTreeWrapper.getAvlTree();
    assertThat(avlTree.getValue(key).getData()).isEqualTo(avlTreeWrapper.getValue(key.getData()));
  }

  @Test
  void serializableConstructor() {
    AvlTreeWrapper avlTreeWrapper = new AvlTreeWrapper();
    assertThat(avlTreeWrapper.getAvlTree()).isNull();
  }
}
