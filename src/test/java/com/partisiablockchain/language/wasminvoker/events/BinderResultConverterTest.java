package com.partisiablockchain.language.wasminvoker.events;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.binder.BinderEventGroup;
import com.partisiablockchain.binder.BinderInteraction;
import com.partisiablockchain.binder.BinderResult;
import com.partisiablockchain.binder.CallResult;
import com.partisiablockchain.binder.CallReturnValue;
import com.partisiablockchain.language.wasminvoker.WasmState;
import java.util.List;
import org.junit.jupiter.api.Test;

final class BinderResultConverterTest {
  @Test
  void callResultDefaultsToNull() {
    EventResult eventResult = EventResult.create();
    BinderResult<WasmState, BinderInteraction> binderResult = eventResult.toBinderResult(null, 10L);
    assertThat(binderResult.getState()).isNull();
    assertThat(binderResult.getInvocations()).isEmpty();
    assertThat(binderResult.getCallResult()).isNull();
  }

  @Test
  void convertInvocations() {
    var event1 = new EventResult.Event(TestObjects.ADDRESS, new byte[] {1}, 2L, false);
    var event2 = new EventResult.Event(TestObjects.ADDRESS, new byte[] {3}, 4L, true);

    var eventResult = new EventResult(List.of(event1, event2), null, null);
    var binderResult = eventResult.toBinderResult(null, 10L);

    assertThat(binderResult.getState()).isNull();

    assertThat(binderResult.getInvocations()).hasSize(2);

    assertThat(binderResult.getInvocations().get(0).contract).isEqualTo(TestObjects.ADDRESS);
    assertThat(binderResult.getInvocations().get(0).rpc).isEqualTo(new byte[] {1});
    assertThat(binderResult.getInvocations().get(0).effectiveCost).isEqualTo(2L);
    assertThat(binderResult.getInvocations().get(0).costFromContract).isFalse();
    assertThat(binderResult.getInvocations().get(0).originalSender).isFalse();

    assertThat(binderResult.getInvocations().get(1).contract).isEqualTo(TestObjects.ADDRESS);
    assertThat(binderResult.getInvocations().get(1).rpc).isEqualTo(new byte[] {3});
    assertThat(binderResult.getInvocations().get(1).effectiveCost).isEqualTo(4L);
    assertThat(binderResult.getInvocations().get(1).costFromContract).isTrue();
    assertThat(binderResult.getInvocations().get(1).originalSender).isFalse();

    assertThat(binderResult.getCallResult()).isNull();
  }

  @SuppressWarnings("unchecked")
  @Test
  void convertCallbackGroup() {
    byte[] callback = new byte[] {1, 2, 3, 4};
    var event = new EventResult.Event(TestObjects.ADDRESS, new byte[] {1}, 2L, false);
    var callbackGroup = new EventResult.CallbackGroup(List.of(event), callback, 3L, false);
    var eventResult = new EventResult(List.of(), callbackGroup, null);
    var binderResult = eventResult.toBinderResult(null, 10L);

    assertThat(binderResult.getState()).isNull();

    assertThat(binderResult.getInvocations()).isEmpty();

    CallResult callResult = binderResult.getCallResult();
    assertThat(callResult).isInstanceOf(BinderEventGroup.class);
    BinderEventGroup<BinderInteraction> binderEventGroup =
        (BinderEventGroup<BinderInteraction>) callResult;

    assertThat(binderEventGroup.getEvents().get(0).contract).isEqualTo(TestObjects.ADDRESS);
    assertThat(binderEventGroup.getEvents().get(0).rpc).isEqualTo(new byte[] {1});
    assertThat(binderEventGroup.getEvents().get(0).effectiveCost).isEqualTo(2L);
    assertThat(binderEventGroup.getEvents().get(0).costFromContract).isFalse();

    assertThat(binderEventGroup.getCallbackRpc()).hasSize(4).containsExactly((byte) 1, 2, 3, 4);
    assertThat(binderEventGroup.getCallbackCost()).isEqualTo(3L);
    assertThat(binderEventGroup.isCallbackCostFromContract()).isFalse();
  }

  @Test
  void convertReturnValue() {
    var returnValue = new CallReturnValue(new byte[] {1, 2, 3});
    var eventResult = new EventResult(List.of(), null, returnValue);
    var binderResult = eventResult.toBinderResult(null, 10L);

    assertThat(binderResult.getState()).isNull();

    assertThat(binderResult.getInvocations()).isEmpty();

    CallResult callResult = binderResult.getCallResult();
    assertThat(callResult).isInstanceOf(CallReturnValue.class);
    CallReturnValue callReturnValue = (CallReturnValue) callResult;

    assertThat(callReturnValue).isEqualTo(returnValue);
  }
}
