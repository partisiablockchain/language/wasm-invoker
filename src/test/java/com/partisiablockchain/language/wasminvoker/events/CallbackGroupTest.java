package com.partisiablockchain.language.wasminvoker.events;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.List;
import org.junit.jupiter.api.Test;

final class CallbackGroupTest {
  static final List<EventResult.Event> DEFAULT_EVENTS =
      List.of(new EventResult.Event(TestObjects.ADDRESS, null, 0L, false));
  static final byte[] DEFAULT_RPC = new byte[0];
  static final long DEFAULT_COST = 0L;
  static final boolean DEFAULT_COST_FROM_CONTRACT = false;

  @Test
  void create() {
    var callbackGroup =
        new EventResult.CallbackGroup(
            DEFAULT_EVENTS, DEFAULT_RPC, DEFAULT_COST, DEFAULT_COST_FROM_CONTRACT);
    assertThat(callbackGroup.events()).isEqualTo(DEFAULT_EVENTS);
    assertThat(callbackGroup.callbackRpc()).isEqualTo(DEFAULT_RPC);
    assertThat(callbackGroup.callbackCost()).isEqualTo(DEFAULT_COST);
    assertThat(callbackGroup.costFromContract()).isEqualTo(DEFAULT_COST_FROM_CONTRACT);
  }

  @Test
  void emptyInvocations() {
    assertThatThrownBy(
            () ->
                new EventResult.CallbackGroup(
                    List.of(), DEFAULT_RPC, DEFAULT_COST, DEFAULT_COST_FROM_CONTRACT))
        .hasMessage("Empty event list is not allowed");
  }

  @Test
  void nullCallback() {
    assertThatThrownBy(
            () ->
                new EventResult.CallbackGroup(
                    DEFAULT_EVENTS, null, DEFAULT_COST, DEFAULT_COST_FROM_CONTRACT))
        .isInstanceOf(NullPointerException.class);
  }

  @Test
  void negativeCost() {
    assertThatThrownBy(
            () ->
                new EventResult.CallbackGroup(
                    DEFAULT_EVENTS, DEFAULT_RPC, -1L, DEFAULT_COST_FROM_CONTRACT))
        .hasMessageStartingWith("Negative");
  }
}
