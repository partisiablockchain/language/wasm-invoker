package com.partisiablockchain.language.wasminvoker.events;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.List;
import org.junit.jupiter.api.Test;

final class EventCostCalculatorTest {
  @Test
  void addCostSums() {
    var lhs = new EventCostCalculator.CostSum(11, 5);
    var rhs = new EventCostCalculator.CostSum(13, 7);

    assertThat(lhs.add(rhs)).isEqualTo(new EventCostCalculator.CostSum(24, 12));
  }

  @Test
  void calculateForCallbackGroupWithCost() {
    var event = new EventResult.Event(TestObjects.ADDRESS, TestObjects.RPC, 0L, false);
    var callbackGroup =
        new EventResult.CallbackGroup(List.of(event), CallbackGroupTest.DEFAULT_RPC, 42L, false);
    var expected = new EventCostCalculator.CostSum(42, 0);
    assertThat(EventCostCalculator.calculateCost(callbackGroup)).isEqualTo(expected);
  }

  @Test
  void calculateForCallbackGroupWithNoCost() {
    var event = new EventResult.Event(TestObjects.ADDRESS, TestObjects.RPC, 0L, false);
    var callbackGroup = new EventResult.CallbackGroup(List.of(event), TestObjects.RPC, null, false);
    var expected = new EventCostCalculator.CostSum(0, 1);
    assertThat(EventCostCalculator.calculateCost(callbackGroup)).isEqualTo(expected);
  }

  @Test
  void calculateForEvents() {
    var eventWithAllocatedCost =
        new EventResult.Event(TestObjects.ADDRESS, TestObjects.RPC, 43L, false);
    var eventWithoutAllocatedCost =
        new EventResult.Event(TestObjects.ADDRESS, TestObjects.RPC, null, false);
    var events = List.of(eventWithAllocatedCost, eventWithoutAllocatedCost);
    var expected = new EventCostCalculator.CostSum(43, 1);
    assertThat(EventCostCalculator.calculateCost(events)).isEqualTo(expected);

    var callBackGroupWithInvocations =
        new EventResult.CallbackGroup(events, TestObjects.RPC, null, false);
    expected = new EventCostCalculator.CostSum(43, 2);
    assertThat(EventCostCalculator.calculateCost(callBackGroupWithInvocations)).isEqualTo(expected);
  }

  @Test
  void costOfEventsWithCostFromContractNotAdded() {
    var eventWithCostFromSender =
        new EventResult.Event(TestObjects.ADDRESS, TestObjects.RPC, 1L, false);
    var eventWithCostFromContract =
        new EventResult.Event(TestObjects.ADDRESS, TestObjects.RPC, 2L, true);
    var eventWithNullCostFromContract =
        new EventResult.Event(TestObjects.ADDRESS, TestObjects.RPC, null, true);

    var events =
        List.of(eventWithCostFromContract, eventWithCostFromSender, eventWithNullCostFromContract);
    var expected = new EventCostCalculator.CostSum(1L, 0);
    assertThat(EventCostCalculator.calculateCost(events)).isEqualTo(expected);

    var callBackGroupWithInvocations =
        new EventResult.CallbackGroup(events, TestObjects.RPC, 3L, true);
    expected = new EventCostCalculator.CostSum(1L, 0);
    assertThat(EventCostCalculator.calculateCost(callBackGroupWithInvocations)).isEqualTo(expected);
  }

  @Test
  void notEnoughAllocatedCost() {
    var callbackGroup =
        new EventResult.CallbackGroup(
            CallbackGroupTest.DEFAULT_EVENTS, TestObjects.RPC, 10L, false);
    EventResult eventResult = new EventResult(List.of(), callbackGroup, null);

    assertThatThrownBy(() -> EventCostCalculator.calculateGasCostIfUnallocated(eventResult, 9L))
        .hasMessageStartingWith("Cannot allocate gas for events");

    long perEventCost = EventCostCalculator.calculateGasCostIfUnallocated(eventResult, 10L);
    assertThat(perEventCost).isEqualTo(0L);
  }
}
