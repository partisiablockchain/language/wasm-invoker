package com.partisiablockchain.language.wasminvoker.events;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import org.junit.jupiter.api.Test;

final class EventTest {
  @Test
  void nullValuesAllowed() {
    var event = new EventResult.Event(null, null, null, false);
    assertThat(event.contract()).isNull();
    assertThat(event.rpc()).isNull();
    assertThat(event.cost()).isNull();
    assertThat(event.costFromContract()).isFalse();
  }

  @Test
  void costMethods() {
    var eventWithoutAllocatedCost =
        new EventResult.Event(TestObjects.ADDRESS, TestObjects.RPC, null, false);

    assertThat(eventWithoutAllocatedCost.hasNullCost()).isTrue();
    assertThat(eventWithoutAllocatedCost.getCostOrZero()).isEqualTo(0);

    long allocatedCost = 13L;
    var eventWithAllocatedCost =
        new EventResult.Event(TestObjects.ADDRESS, TestObjects.RPC, allocatedCost, false);

    assertThat(eventWithAllocatedCost.hasNullCost()).isFalse();
    assertThat(eventWithAllocatedCost.getCostOrZero()).isEqualTo(allocatedCost);
  }

  @Test
  void negativeCost() {
    assertThatThrownBy(
            () -> new EventResult.Event(TestObjects.ADDRESS, TestObjects.RPC, -1L, false))
        .hasMessage("Negative cost is not allowed");
  }
}
