package com.partisiablockchain.language.wasminvoker.events;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.binder.BinderEventGroup;
import com.partisiablockchain.binder.BinderInteraction;
import java.util.List;
import org.junit.jupiter.api.Test;

/** Tests the fee distribution that happens when converting to BinderResult in EventResult. */
final class FeeDistributionTest {
  @Test
  void allocationHappensForInvocations() {
    EventResult.Event eventWithoutAllocatedCost = eventWithCost(null);

    long allocatedCost = 13L;
    EventResult.Event eventWithAllocatedCost = eventWithCost(allocatedCost);

    var eventResult =
        new EventResult(List.of(eventWithAllocatedCost, eventWithoutAllocatedCost), null, null);

    long gasCostIfUnallocated = EventCostCalculator.calculateGasCostIfUnallocated(eventResult, 42L);
    assertThat(gasCostIfUnallocated).isEqualTo(29L); // (42 - 13), split between one event

    BinderInteraction convertedEventWithoutAllocatedCost =
        BinderResultConverter.toBinderInteraction(eventWithoutAllocatedCost, gasCostIfUnallocated);

    assertThat(convertedEventWithoutAllocatedCost.effectiveCost).isEqualTo(gasCostIfUnallocated);

    BinderInteraction convertedEventWithAllocatedCost =
        BinderResultConverter.toBinderInteraction(eventWithAllocatedCost, gasCostIfUnallocated);
    assertThat(convertedEventWithAllocatedCost.effectiveCost).isEqualTo(allocatedCost);
  }

  @Test
  void allocationHappensForCallbackGroup() {
    var event1 = eventWithCost(null);
    var event2 = eventWithCost(null);
    var event3 = eventWithCost(3L);

    var group =
        new EventResult.CallbackGroup(
            List.of(event1, event2, event3), TestObjects.RPC, null, false);

    var eventResult = new EventResult(List.of(), group, null);
    long gasCostIfUnallocated = EventCostCalculator.calculateGasCostIfUnallocated(eventResult, 20);
    assertThat(gasCostIfUnallocated)
        .isEqualTo(5L); // (20 - 3), split between 2 events and 1 callback cost

    BinderEventGroup<BinderInteraction> eventGroup =
        BinderResultConverter.toBinderEventGroup(group, gasCostIfUnallocated);
    assertThat(eventGroup.getCallbackCost()).isEqualTo(gasCostIfUnallocated);
    assertThat(eventGroup.getEvents()).hasSize(3);
    assertThat(eventGroup.getEvents().get(0).effectiveCost).isEqualTo(gasCostIfUnallocated);
    assertThat(eventGroup.getEvents().get(1).effectiveCost).isEqualTo(gasCostIfUnallocated);
    assertThat(eventGroup.getEvents().get(2).effectiveCost).isEqualTo(3L);
  }

  @Test
  void allocationHappensForListAndCallbackGroup() {
    var event1 = eventWithCost(null);
    var event2 = eventWithCost(null);

    List<EventResult.Event> invocations = List.of(event1);
    var group = new EventResult.CallbackGroup(List.of(event2), TestObjects.RPC, null, false);

    var eventResult = new EventResult(invocations, group, null);
    long gasCostIfUnallocated = EventCostCalculator.calculateGasCostIfUnallocated(eventResult, 20L);
    assertThat(gasCostIfUnallocated)
        .isEqualTo(6L); // 20, split between 2 events and 1 callback cost

    List<BinderInteraction> convertedInvocations =
        BinderResultConverter.toBinderInteractions(invocations, gasCostIfUnallocated);
    assertThat(convertedInvocations.get(0).effectiveCost).isEqualTo(gasCostIfUnallocated);
    BinderEventGroup<BinderInteraction> convertedEventGroup =
        BinderResultConverter.toBinderEventGroup(group, gasCostIfUnallocated);
    assertThat(convertedEventGroup.getCallbackCost()).isEqualTo(gasCostIfUnallocated);
    assertThat(convertedEventGroup.getEvents().get(0).effectiveCost)
        .isEqualTo(gasCostIfUnallocated);
  }

  private static EventResult.Event eventWithCost(Long cost) {
    return new EventResult.Event(TestObjects.ADDRESS, TestObjects.RPC, cost, false);
  }
}
