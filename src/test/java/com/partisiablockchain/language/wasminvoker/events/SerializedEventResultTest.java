package com.partisiablockchain.language.wasminvoker.events;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.wasminvoker.Configuration;
import com.secata.stream.SafeDataInputStream;
import java.util.List;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.Test;

final class SerializedEventResultTest {

  private final Configuration noSupportCostFromContract = new Configuration(true, false);
  private final Configuration supportCostFromContract = new Configuration(true, true);

  @Test
  void readInteraction() {
    byte[] bytes = Hex.decode(eventAsString("ABCD", "13", null));
    SafeDataInputStream in = SafeDataInputStream.createFromBytes(bytes);
    EventResult.Event interaction = SerializedEventResult.readEvent(noSupportCostFromContract, in);
    assertInteraction(interaction);
  }

  static final String ILLEGAL_INTERACTION =
      ""
          + TestObjects.ADDRESS.writeAsString()
          + "00000002" // RPC length as BE
          + "ABCD" // RPC
          + "01" // Send from original sender (illegal)
          + "01" // Costs exists
          + "00000000000000"
          + "13"; // the cost

  @Test
  void readInteractionWithSendFromOriginal() {
    byte[] bytes = Hex.decode(ILLEGAL_INTERACTION);
    SafeDataInputStream in = SafeDataInputStream.createFromBytes(bytes);
    assertThatThrownBy(() -> SerializedEventResult.readEvent(noSupportCostFromContract, in))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Sending from original sender is not supported");
  }

  static final String INVALID_GROUP =
      ""
          + "00000001" // One group
          + "00" // Null callbackRpc
          + "01" // Non-null callback cost
          + "0000000000000020" // Callback cost
          + "00000000" // Zero interactions
          + "00"; // None return data;

  @Test
  void nullCallbackButNonNullCallbackCostCreatesNullCallbackGroup() {
    EventResult eventResult =
        readEventResultFromHexString(noSupportCostFromContract, INVALID_GROUP);
    assertThat(eventResult.getCallbackGroup()).isNull();
  }

  static void assertInteraction(EventResult.Event event) {
    assertThat(event.contract()).isEqualTo(TestObjects.ADDRESS);
    assertThat(event.rpc()).containsExactly((byte) 0xAB, 0xCD);
    assertThat(event.cost()).isEqualTo(0x13);
  }

  static final String EVENT_GROUP_WITH_CALLBACK_AND_RETURN_DATA =
      ""
          + "00000001" // One group
          + "01" // Non-null callbackRpc
          + "00000004" // Four bytes callbackRpc
          + "AFBFCFDF" // Callback contents
          + "01" // Non-null callback cost
          + "0000000000000020" // Callback cost
          + "00000001" // One interaction
          + eventAsString("ABCD", null, null)
          + "01" // Non-null return data;
          + "00000004" // Number of bytes to read
          + "00000010"; // return data

  @Test
  void cannotSerializeEventGroupWithBothCallbackAndReturnData() {
    assertThatThrownBy(
            () ->
                readEventResultFromHexString(
                    noSupportCostFromContract, EVENT_GROUP_WITH_CALLBACK_AND_RETURN_DATA))
        .hasMessage("Having both return value and callback group is not allowed.");
  }

  static final String EVENT_GROUP_WITH_CALLBACK_AND_EVENT_GROUP_WITH_RETURN_DATA =
      ""
          + "00000002" // Two groups
          + "00" // null callbackRpc
          + "00" // no callback cost
          + "00000001" // One interaction
          + eventAsString("ABCD", null, null)
          + "01" // Non-null return data;
          + "00000004" // Number of bytes to read
          + "00000010" // return data
          + "01" // Non-null callbackRpc
          + "00000004" // Four bytes callbackRpc
          + "AFBFCFDF" // Callback contents
          + "01" // Non-null callback cost
          + "0000000000000020" // Callback cost
          + "00000001" // One interaction          3
          + eventAsString("ABCD", null, null)
          + "00"; // None return data

  @Test
  void cannotSerializeEventGroupWithReturnDataAndEventGroupWithCallback() {
    assertThatThrownBy(
            () ->
                readEventResultFromHexString(
                    noSupportCostFromContract,
                    EVENT_GROUP_WITH_CALLBACK_AND_EVENT_GROUP_WITH_RETURN_DATA))
        .hasMessage("Having both return value and callback group is not allowed.");
  }

  static final String TWO_EVENT_GROUPS_WITH_CALLBACK =
      ""
          + "00000002" // Two groups
          + "01" // Non-null callbackRpc
          + "00000004" // Four bytes callbackRpc
          + "AFBFCFDF" // Callback contents
          + "01" // Non-null callback cost
          + "0000000000000020" // Callback cost
          + "00000001" // One interaction
          + eventAsString("ABCD", "13", null)
          + "00" // None return data
          + "01" // Non-null callbackRpc
          + "00000004" // Four bytes callbackRpc
          + "AFBFCFDF" // Callback contents
          + "01" // Non-null callback cost
          + "0000000000000020" // Callback cost
          + "00000001" // One interaction
          + eventAsString("ABCD", "13", null)
          + "00"; // None return data

  @Test
  void cannotSerializeTwoEventGroupsWithCallback() {
    assertThatThrownBy(
            () ->
                readEventResultFromHexString(
                    noSupportCostFromContract, TWO_EVENT_GROUPS_WITH_CALLBACK))
        .hasMessage("Multiple callback groups are not allowed");
  }

  static final String TWO_EVENT_GROUPS_WITH_RETURN_DATA =
      ""
          + "00000002" // Two groups
          + "00" // null callbackRpc
          + "00" // no callback cost
          + "00000000" // One interaction
          + "01" // Non-null return data;
          + "00000004" // Number of bytes to read
          + "00000010" // return data
          + "00" // null callbackRpc
          + "00" // no callback cost
          + "00000000" // One interaction
          + "01" // Non-null return data;
          + "00000004" // Number of bytes to read
          + "00000010"; // return data

  @Test
  void cannotSerializeTwoEventGroupsWithReturnData() {
    assertThatThrownBy(
            () ->
                readEventResultFromHexString(
                    noSupportCostFromContract, TWO_EVENT_GROUPS_WITH_RETURN_DATA))
        .hasMessage("Multiple return values are not allowed");
  }

  static final String TWO_EVENT_GROUPS =
      ""
          + "00000002" // Two groups
          + "00" // null callbackRpc
          + "00" // no callback cost
          + "00000001" // One interaction
          + eventAsString("ABCD", null, null)
          + "00" // None return data
          + "00" // null callbackRpc
          + "00" // no callback cost
          + "00000002" // Two interactions
          + eventAsString("ABCD", null, null)
          + eventAsString("DE", null, null)
          + "00"; // None return data

  @Test
  void serializeMultipleEventGroups() {
    var event1 = new EventResult.Event(TestObjects.ADDRESS, Hex.decode("ABCD"), null, false);
    var event2 = new EventResult.Event(TestObjects.ADDRESS, Hex.decode("DE"), null, false);

    EventResult eventResult =
        readEventResultFromHexString(noSupportCostFromContract, TWO_EVENT_GROUPS);
    List<EventResult.Event> expected = List.of(event1, event1, event2);
    assertThat(eventResult.getInvocations()).usingRecursiveComparison().isEqualTo(expected);
  }

  static final String EVENT_GROUP_WITH_CALLBACK_ONE_EVENT =
      ""
          + "00000001" // One group
          + "01" // Non-null callbackRpc
          + "00000004" // Four bytes callbackRpc
          + "AFBFCFDF" // Callback contents
          + "01" // Non-null callback cost
          + "0000000000000020" // Callback cost
          + "00000001" // One interaction
          + eventAsString("ABCD", "13", null)
          + "00"; // None return data

  @Test
  void serializeNormalCallbackGroup() {
    EventResult eventResult =
        readEventResultFromHexString(
            noSupportCostFromContract, EVENT_GROUP_WITH_CALLBACK_ONE_EVENT);
    assertThat(eventResult.getCallbackGroup().events()).hasSize(1);
    assertThat(eventResult.getCallbackGroup().callbackRpc())
        .containsExactly((byte) 0xAF, 0xBF, 0xCF, 0xDF);

    assertThat(eventResult.getCallbackGroup().events()).hasSize(1);
    assertInteraction(eventResult.getCallbackGroup().events().get(0));

    assertThat(eventResult.getInvocations()).isEmpty();
  }

  static final String EVENT_GROUP_WITH_RETURN_DATA_ONE_EVENT =
      ""
          + "00000001" // One group
          + "00" // null callbackRpc
          + "00" // no callback cost
          + "00000001" // One interaction
          + eventAsString("ABCD", "08", null)
          + "01" // Non-null return data;
          + "00000004" // Number of bytes to read
          + "00000010"; // return data

  @Test
  void shouldCreateCallReturnValueGivenReturnDataAndEvents() {
    var event = new EventResult.Event(TestObjects.ADDRESS, Hex.decode("ABCD"), 8L, false);
    var result = Hex.decode("00000010");

    var eventResult =
        readEventResultFromHexString(
            noSupportCostFromContract, EVENT_GROUP_WITH_RETURN_DATA_ONE_EVENT);

    assertThat(eventResult.getInvocations()).hasSize(1);
    assertThat(eventResult.getInvocations().get(0)).usingRecursiveComparison().isEqualTo(event);
    assertThat(eventResult.getCallbackGroup()).isNull();
    assertThat(eventResult.getCallReturnValue().getResult()).isEqualTo(result);
  }

  static final String EMPTY_EVENT_GROUP =
      ""
          + "00000001" // One group
          + "00" // null callbackRpc
          + "00" // null callback cost
          + "00000000" // No interactions
          + "00"; // null return data;

  @Test
  void shouldBehaveWellOnEmptyEventGroup() {
    var eventResult = readEventResultFromHexString(noSupportCostFromContract, EMPTY_EVENT_GROUP);

    assertThat(eventResult.getInvocations()).isEmpty();
    assertThat(eventResult.getCallbackGroup()).isNull();
    assertThat(eventResult.getCallReturnValue()).isNull();
  }

  static final String EVENT_GROUP_WITH_CALLBACK_NO_EVENTS =
      ""
          + "00000001" // One group
          + "01" // non-null callbackRpc
          + "00000004" // Four bytes callbackRpc
          + "AFBFCFDF" // Callback contents
          + "01" // Non-null callback cost
          + "0000000000000020" // Callback cost
          + "00000000" // Zero interactions
          + "00"; // None return data;

  @Test
  void shouldFailWithCallbackHavingNoEvents() {
    assertThatThrownBy(
            () ->
                readEventResultFromHexString(
                    noSupportCostFromContract, EVENT_GROUP_WITH_CALLBACK_NO_EVENTS))
        .hasMessage("Empty event list is not allowed");
  }

  @Test
  void readInteractionCostFromContractTrue() {
    byte[] bytes = Hex.decode(eventAsString("ABCD", "13", true));
    SafeDataInputStream in = SafeDataInputStream.createFromBytes(bytes);
    EventResult.Event interaction = SerializedEventResult.readEvent(supportCostFromContract, in);
    assertInteraction(interaction);
    assertThat(interaction.costFromContract()).isTrue();
  }

  @Test
  void readInteractionCostFromContractFalse() {
    byte[] bytes = Hex.decode(eventAsString("ABCD", "13", false));
    SafeDataInputStream in = SafeDataInputStream.createFromBytes(bytes);
    EventResult.Event interaction = SerializedEventResult.readEvent(supportCostFromContract, in);
    assertInteraction(interaction);
    assertThat(interaction.costFromContract()).isFalse();
  }

  static final String EVENT_GROUP_WITH_TWO_EVENTS_COST_FROM_CONTRACT =
      ""
          + "00000001" // One group
          + "00" // null callbackRpc
          + "00" // no callback cost
          + "00000002" // One interaction
          + eventAsString("ABCD", "08", true)
          + eventAsString("0123", "13", false)
          + "00"; // Non-null return data

  @Test
  void readCostFromContract() {
    var event1 = new EventResult.Event(TestObjects.ADDRESS, Hex.decode("ABCD"), 8L, true);
    var event2 = new EventResult.Event(TestObjects.ADDRESS, Hex.decode("0123"), 0x13L, false);

    var eventResult =
        readEventResultFromHexString(
            supportCostFromContract, EVENT_GROUP_WITH_TWO_EVENTS_COST_FROM_CONTRACT);

    assertThat(eventResult.getInvocations()).hasSize(2);
    assertThat(eventResult.getInvocations().get(0)).usingRecursiveComparison().isEqualTo(event1);
    assertThat(eventResult.getInvocations().get(1)).usingRecursiveComparison().isEqualTo(event2);
    assertThat(eventResult.getCallbackGroup()).isNull();
    assertThat(eventResult.getCallReturnValue()).isNull();
  }

  private static String eventAsString(String rpcAsHex, String costAsHex, Boolean costFromContract) {
    String rpcLength = "0" + Hex.decode(rpcAsHex).length;
    String base =
        TestObjects.ADDRESS.writeAsString()
            + "000000" // first bytes of rpc length
            + rpcLength // actual length (1 byte)
            + rpcAsHex // RPC
            + "00"; // Send from contract
    if (costAsHex == null) {
      base = base + "00"; // No cost exists
    } else {
      base =
          base
              + "01" // Costs exists
              + "00000000000000"
              + costAsHex; // the cost
    }
    if (costFromContract == null) {
      return base;
    } else {
      return base + (costFromContract ? "01" : "00");
    }
  }

  static EventResult readEventResultFromHexString(Configuration configuration, String hexString) {
    byte[] bytes = Hex.decode(hexString);
    return SafeDataInputStream.readFully(
        bytes,
        safeDataInputStream ->
            SerializedEventResult.readEventResultFromStream(configuration, safeDataInputStream));
  }
}
