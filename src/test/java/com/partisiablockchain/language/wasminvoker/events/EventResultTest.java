package com.partisiablockchain.language.wasminvoker.events;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.binder.CallReturnValue;
import java.util.List;
import org.junit.jupiter.api.Test;

final class EventResultTest {
  @Test
  void emptyEventResult() {
    EventResult empty = EventResult.create();
    assertThat(empty.getInvocations()).isNotNull();
    assertThat(empty.getInvocations()).isEmpty();
    assertThat(empty.getCallbackGroup()).isNull();
    assertThat(empty.getCallReturnValue()).isNull();
  }

  @Test
  void addEvent() {
    var event1 = new EventResult.Event(TestObjects.ADDRESS, new byte[1], 1L, false);
    var event2 = new EventResult.Event(TestObjects.ADDRESS, new byte[2], 2L, true);

    var list = List.of(event1);
    EventResult eventResult = new EventResult(list, null, null);

    assertThat(eventResult.getInvocations()).usingRecursiveComparison().isEqualTo(list);

    eventResult.addEventWithCostFromContract(TestObjects.ADDRESS, new byte[2], 2L);

    assertThat(eventResult.getInvocations())
        .usingRecursiveComparison()
        .isEqualTo(List.of(event1, event2));
  }

  @Test
  void setResult() {
    EventResult eventResult = EventResult.create();
    assertThat(eventResult.getCallReturnValue()).isNull();
    var returnValue = new byte[] {1, 2, 3};
    eventResult.setReturnValue(returnValue);
    assertThat(eventResult.getCallReturnValue().getResult()).isEqualTo(returnValue);
  }

  @Test
  void cannotSetResultWhenCallbackGroupExists() {
    var event = new EventResult.Event(TestObjects.ADDRESS, TestObjects.RPC, null, false);
    var callbackGroup = new EventResult.CallbackGroup(List.of(event), TestObjects.RPC, null, false);
    var eventResult = new EventResult(List.of(), callbackGroup, null);
    assertThatThrownBy(() -> eventResult.setReturnValue(new byte[3]))
        .hasMessage("Having both return value and callback group is not allowed.");
  }

  @Test
  void cannotSetResultWhenAlreadySet() {
    var returnValue = new byte[1];
    var eventResult = new EventResult(List.of(), null, new CallReturnValue(returnValue));
    assertThatThrownBy(() -> eventResult.setReturnValue(new byte[2]))
        .hasMessage("Cannot overwrite return value");
  }
}
