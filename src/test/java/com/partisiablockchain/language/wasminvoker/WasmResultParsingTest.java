package com.partisiablockchain.language.wasminvoker;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.binder.BinderInteraction;
import com.partisiablockchain.binder.BinderResult;
import com.partisiablockchain.language.wasminvoker.events.BinderResultConverter;
import org.junit.jupiter.api.Test;

/** Test of {@link WasmInvokerImpl}. */
public final class WasmResultParsingTest {

  private static final long gasBeforeWasmExecution = 5000;
  private static final long gasUsedDuringWasmExecution = 1213;

  private final Configuration testConfiguration = new Configuration(true, false);

  @Test
  public void createEmptyResult() {
    final byte[] bytes = new byte[0];
    final WasmInvoker.WasmResult result =
        WasmInvokerImpl.createResult(testConfiguration, gasUsedDuringWasmExecution, bytes, null);
    assertThat(result.usedWasmGas()).isEqualTo(gasUsedDuringWasmExecution);
    assertThat(result.state().getState()).isEmpty();
    BinderResult<WasmState, BinderInteraction> finalizedResult =
        toBinderResult(result, gasBeforeWasmExecution);
    assertThat(finalizedResult.getState().getState()).isEmpty();
    assertThat(finalizedResult.getInvocations().size()).isEqualTo(0);
  }

  @Test
  public void createStateResult() {
    final byte[] bytes =
        new byte[] {
          1, // State section
          0, 0, 0, 4, // State section length
          1, 2, 3, 4, // State data
        };
    final WasmInvoker.WasmResult result =
        WasmInvokerImpl.createResult(testConfiguration, gasUsedDuringWasmExecution, bytes, null);
    assertThat(result.usedWasmGas()).isEqualTo(gasUsedDuringWasmExecution);
    assertThat(result.state().getState()).containsExactly((byte) 1, 2, 3, 4);
    BinderResult<WasmState, BinderInteraction> finalizedResult =
        toBinderResult(result, gasBeforeWasmExecution);
    assertThat(finalizedResult.getState().getState()).containsExactly((byte) 1, 2, 3, 4);
    assertThat(finalizedResult.getInvocations().size()).isEqualTo(0);
  }

  @Test
  public void createEmptyStateResult() {
    final byte[] bytes =
        new byte[] {
          1, // State section
          0, 0, 0, 0, // State section length
        };
    final WasmInvoker.WasmResult result =
        WasmInvokerImpl.createResult(testConfiguration, gasUsedDuringWasmExecution, bytes, null);
    assertThat(result.usedWasmGas()).isEqualTo(gasUsedDuringWasmExecution);
    assertThat(result.state().getState()).isEmpty();
    BinderResult<WasmState, BinderInteraction> finalizedResult =
        toBinderResult(result, gasBeforeWasmExecution);
    assertThat(finalizedResult.getState().getState()).isEmpty();
    assertThat(finalizedResult.getInvocations().size()).isEqualTo(0);
  }

  @Test
  public void createStateAndUnknownResult() {
    final byte[] bytes =
        new byte[] {
          1, // State section
          0, 0, 0, 4, // State section length
          1, 2, 3, 4, // State data
          55, // Unknown section
          0, 0, 0, 4, // Unknown section length
          12, 21, 23, 32, // Unknown section data
        };
    final WasmInvoker.WasmResult result =
        WasmInvokerImpl.createResult(testConfiguration, gasUsedDuringWasmExecution, bytes, null);
    assertThat(result.state().getState()).containsExactly((byte) 1, 2, 3, 4);
    assertThat(result.sections().size()).isEqualTo(1);
    assertThat(result.getSection((byte) 55).sectionId()).isEqualTo((byte) 55);
    assertThat(result.getSection((byte) 55).sectionData()).containsExactly((byte) 12, 21, 23, 32);
    BinderResult<WasmState, BinderInteraction> finalizedResult =
        toBinderResult(result, gasBeforeWasmExecution);
    assertThat(finalizedResult.getState().getState()).containsExactly((byte) 1, 2, 3, 4);
  }

  @Test
  public void duplicatedStateResult() {
    final byte[] bytes =
        new byte[] {
          1, // State section
          0, 0, 0, 4, // State section length
          1, 2, 3, 4, // State data
          1, // State section 2
          0, 0, 0, 4, // State section length 2
          1, 2, 3, 4, // State data 2
        };
    assertThatThrownBy(
            () ->
                WasmInvokerImpl.createResult(
                    testConfiguration, gasUsedDuringWasmExecution, bytes, null))
        .hasMessage(
            "Invalid section 1: Duplicated or incorrectly ordered. Expected section with id"
                + " of at least 2");
  }

  @Test
  public void explicitEventsSection() {
    final byte[] bytes =
        new byte[] {
          1, // State section 2
          0, 0, 0, 4, // State section length 2
          1, 2, 3, 4, // State data 2
          2, // Events section
          0, 0, 0, 4, // Events  section length
          0, 0, 0, 0, // Events data (length: 0)
        };
    final WasmInvoker.WasmResult result =
        WasmInvokerImpl.createResult(testConfiguration, gasUsedDuringWasmExecution, bytes, null);
    assertThat(result.state().getState()).containsExactly((byte) 1, 2, 3, 4);
    BinderResult<WasmState, BinderInteraction> finalizedResult =
        toBinderResult(result, gasBeforeWasmExecution);
    assertThat(finalizedResult.getInvocations().size()).isEqualTo(0);
    assertThat(result.sections().size()).isEqualTo(0);
  }

  @Test
  void actualEventsSection() {
    final byte[] bytes =
        new byte[] {
          2, // event section
          0, 0, 0, 38, // event section length
          0, 0, 0, 1, // one group
          0, // null callbackRpc
          0, // no callback cost
          0, 0, 0, 1, // one interaction
          1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // address
          0, 0, 0, 0, // rpc length
          0, // send from contract
          0, // no cost
          0, // none return data
        };

    final WasmInvoker.WasmResult result =
        WasmInvokerImpl.createResult(testConfiguration, gasUsedDuringWasmExecution, bytes, null);
    BinderResult<WasmState, BinderInteraction> finalizedResult =
        toBinderResult(result, gasBeforeWasmExecution);
    assertThat(finalizedResult.getInvocations()).hasSize(1);
    BinderInteraction interaction = finalizedResult.getInvocations().get(0);
    assertThat(interaction.rpc).isEmpty();
    assertThat(finalizedResult.getCallResult()).isNull();
  }

  @Test
  public void createTwoDifferentUnknownSections() {
    final byte[] bytes =
        new byte[] {
          55, // Unknown  section 1
          0, 0, 0, 4, // Unknown section 1 length
          92, 91, 93, 92, // Unknown section 1 data
          58, // Unknown section 2
          0, 0, 0, 4, // Unknown section 2 length
          12, 21, 23, 32, // Unknown section 2 data
        };
    final WasmInvoker.WasmResult result =
        WasmInvokerImpl.createResult(testConfiguration, gasUsedDuringWasmExecution, bytes, null);
    assertThat(result.state().getState()).isEmpty();
    BinderResult<WasmState, BinderInteraction> finalizedResult =
        toBinderResult(result, gasBeforeWasmExecution);
    assertThat(finalizedResult.getInvocations().size()).isEqualTo(0);

    assertThat(result.sections().size()).isEqualTo(2);
    assertThat(result.getSection((byte) 55).sectionId()).isEqualTo((byte) 55);
    assertThat(result.getSection((byte) 55).sectionData()).containsExactly((byte) 92, 91, 93, 92);
    assertThat(result.getSection((byte) 58).sectionId()).isEqualTo((byte) 58);
    assertThat(result.getSection((byte) 58).sectionData()).containsExactly((byte) 12, 21, 23, 32);
    assertThat(result.getSection((byte) 99)).isNull();
  }

  @Test
  public void failOnNegativeLength() {
    final byte[] bytes =
        new byte[] {
          55, // Unknown  section 1
          -1, -1, -1, -1, // Unknown section 1 length
          92, 91, 93, 92, // Unknown section 1 data
        };

    assertThatThrownBy(
            () ->
                WasmInvokerImpl.createResult(
                    testConfiguration, gasUsedDuringWasmExecution, bytes, null))
        .hasMessage("Invalid section 55: Section length -1 is negative");
  }

  @Test
  public void failOnLargeLength() {
    final byte[] bytes =
        new byte[] {
          55, // Unknown  section 1
          0, 0, 0, 5, // Unknown section 1 length
          92, 91, 93, 92, // Unknown section 1 data
        };

    assertThatThrownBy(
            () ->
                WasmInvokerImpl.createResult(
                    testConfiguration, gasUsedDuringWasmExecution, bytes, null))
        .hasMessage("Invalid section 55: Section length 5 longer than remaining data");
  }

  @Test
  void eventSectionCostFromContract() {
    final byte[] bytes =
        new byte[] {
          2, // event section
          0, 0, 0, 39, // event section length
          0, 0, 0, 1, // one group
          0, // null callbackRpc
          0, // no callback cost
          0, 0, 0, 1, // one interaction
          1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // address
          0, 0, 0, 0, // rpc length
          0, // send from contract
          0, // no cost
          1, // Cost from contract
          0, // none return data
        };

    final WasmInvoker.WasmResult result =
        WasmInvokerImpl.createResult(
            new Configuration(true, true), gasUsedDuringWasmExecution, bytes, null);
    BinderResult<WasmState, BinderInteraction> finalizedResult =
        toBinderResult(result, gasBeforeWasmExecution);
    assertThat(finalizedResult.getInvocations()).hasSize(1);
    BinderInteraction interaction = finalizedResult.getInvocations().get(0);
    assertThat(interaction.rpc).isEmpty();
    assertThat(interaction.costFromContract).isTrue();
    assertThat(finalizedResult.getCallResult()).isNull();
  }

  /**
   * Converts a WasmResult to a BinderResult.
   *
   * @param gasBeforeWasmExecution the available gas before the wasm invocation creating this result
   *     was executed
   * @return the converted result
   */
  static BinderResult<WasmState, BinderInteraction> toBinderResult(
      WasmInvoker.WasmResult wasmResult, long gasBeforeWasmExecution) {
    return BinderResultConverter.toBinderResult(
        wasmResult.eventResult(),
        wasmResult.state(),
        gasBeforeWasmExecution - wasmResult.usedWasmGas());
  }
}
