(module
  (import "pbc" "exit" (func (param i32 i32)))
  (memory 1)
  (data 0 (i32.const 0x00) "Exit string")
  (func $call_exit (param) (result i64)
    i32.const 0 ;; Pointer to 0
    i32.const 11 ;; Size of string
    call 0  ;; Won't return, since it throws exception.
    i64.const 0x0000_0012_0000_0000
  )
  (global $binder_exported i32 i32.const 0)
  (export "__PBC_VERSION_BINDER_8_0_0" (global $binder_exported))
  (export "call_exit" (func $call_exit ))
)
