(module
  (type (;0;) (func (result i64)))
  (data (;0;) (i32.const 0x04) "\01\00\00\00\04WASM")
  (func $noresult (type 0) (result i64)
    i32.const 0
    i32.const 9
    i32.store8 offset=3

    i64.const 0
  )
  (memory 1)
  (export "noresult" (func $noresult))
  (export "__PBC_VERSION_BINDER_8_0_0" (func $noresult))
)
