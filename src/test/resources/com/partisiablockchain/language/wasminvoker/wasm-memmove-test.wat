(module
  (import "pbc" "memmove" (func (param i32 i32 i32) (result i32)))
  (memory 1)
  (data 0 (i32.const 0x00) "\00\00\00\12") ;; Length of the result
  (data 0 (i32.const 0x04) "\01") ;; Section id: state
  (data 0 (i32.const 0x05) "\00\00\00\0d") ;; Section length
  (data 0 (i32.const 0x09) "Hello, World!")
  (func $callmemmove (param) (result i64)
    i32.const 0x10 ;; Src = position of "Hello" + 7
    i32.const 0x09 ;; Dest = start pos of "Hello"
    i32.const 5    ;; Length: |Hello|
    call 0  ;; Memory should now be: "Hello, Hello!"
    drop

    i64.const 0
  )
  (global $binder_exported i32 i32.const 0)
  (export "__PBC_VERSION_BINDER_8_0_0" (global $binder_exported))
  (export "call_memmove" (func $callmemmove ))
)

